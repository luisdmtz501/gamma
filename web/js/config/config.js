let url = false;
let refUrlPag = false;
//let conekta = false;
//let configFireBase = false;

const modoServidor = 'local';
// const modoServidor = 'dev';
// const modoServidor = 'prod';

if (modoServidor === 'local') {
    url = `http://localhost/gamma/backend/public/`;
    refUrlPag = `http://localhost/gamma/web/`;
} else if (modoServidor === 'dev') {
    url = ` `;
    refUrlPag = ` `;
} else if (modoServidor === 'prod') {
    url = `http://huauchitour.com/backend/public/`;
    refUrlPag = `http://huauchitour.com/`;

}

export {
    url,
    refUrlPag
}