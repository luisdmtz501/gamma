import Api from '../../js/api/api.js'
import {url,refUrlPag} from '../../js/config/config.js';

export const login = (usuario,password) =>{
    
    let servicio = `${url}auth/admin`;
    let parametros = {
        "User": usuario,
        "Password": password,
        "tipopersona": 3
    };
    let api = new Api(servicio,"POST",parametros);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            sessionStorage.setItem('tokenAdmin',resultado.result.token);
            self.location = "index.html"
        }else{
           let modal = document.querySelector("#logoutModal");
           modal.style.display = "block";
           modal.classList.add('show');
        //    cuerpo = document.querySelector("#page-top");
        //    cuerpo.classList.add("modal-open");
           document.querySelector("#backgrondModal").style.display = "block";
        }
    })

    botonLogin.disabled = false;
    spinnerLogin.style.display = "none";
}
export const quitarLoader = () =>{
    // setTimeout(()=>document.getElementById('loaderPage').style.display = 'none', 1000);
    document.getElementById('loaderPage').style.display = 'none';
}
export const auth = () => {
    let token = sessionStorage.getItem('tokenAdmin');
    let servicio = `${url}auth/getData/${token}`
    let api = new Api(servicio,"GET");
    let res = api.call();
    res.then(resultado => {
        if (resultado.response === false) {
            self.location = "login.html"
        }else{
            quitarLoader();
            let nombreAdmin = `${resultado.result.Nombre} ${resultado.result.Apellidos}`;
            document.querySelector(`#nombreAdmin`).innerHTML = nombreAdmin;
        }
    });
} 

export const logout = () => {
    sessionStorage.removeItem("tokenAdmin");
    self.location = "login.html"

}
export const llenarModalLoguot = () => {
    document.querySelector("#modalLabelTitle").innerHTML = "Cerrar sesion";
    document.querySelector("#modalLabelBody").innerHTML = "Esta seguro de cerrar su sesion?";
    document.querySelector("#modalActionButton").innerHTML = "Aceptar";
    document.querySelector("#modalActionButton").addEventListener("click", (e)=>{
        logout();
    })  
}
export const cerraModal = () => {
    let modal = document.querySelector("#logoutModal");
    modal.style.display = "none";
    modal.classList.remove('show');
    cuerpo = document.querySelector("#page-top");
    cuerpo.classList.remove("modal-open");
    document.querySelector("#backgrondModal").style.display = "none";
        
}

// mostrar form
export const mostrarOcultar = (nomelement) => {
    let visible = document.querySelector(`#${nomelement}`).style.display;
    switch (visible) {
        case "none":
            document.querySelector(`#${nomelement}`).style.display = "block";
            break;
        case "block":
            document.querySelector(`#${nomelement}`).style.display = "none";
            break;
    } 
}

// crud admin establecimiento
export const listarAdminEstab = (forselect = false) => {
    let servicio = `${url}user/toList/2`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result.Data;
            if (!forselect) {
                let cuerpo = document.querySelector("#listaAdminEstab");
                window.llenarModalEliminarAdmiEstab = llenarModalEliminarAdmiEstab;
                data.forEach(element => {
                    let fila = document.createElement(`div`)
                    fila.innerHTML = `
                    <div class="col-xl-12 col-md-12 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tel: ${element.Telefono} - Email: ${element.Correo}</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${element.Nombre} ${element.Apellidos}</div>
                            Establecimiento: ${element.NombreEstablecimiento}
                        </div>
                        <div class="col-auto">
                            <a href="agregar-admin.html?id=${element.idPersona}" class="btn btn-info btn-circle">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" onclick="llenarModalEliminarAdmiEstab(${element.idPersona})" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>`;
                    cuerpo.appendChild(fila);
                    document.querySelector(`#eliminar${element.idPersona}`);

                });
            }else{
                let lista = document.querySelector("#administrador");
                    lista.innerHTML += `<option value = "0"> Sin seleccion </option>`;
                data.forEach(element => {
                        lista.innerHTML += `
                        <option value="${element.idPersona}">${element.Nombre} ${element.Apellidos} - ${element.Telefono} </option>
                    `;
                    
                });
            }
            
            
        }else{

        }
        
    });
}

export const agregarAdminEstab = (parametros) => {
    let servicio = `${url}user/registerUser`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"POST",parametros,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Adminstrador agregado de manera exitosa")
            self.location.reload();
        }else{
            alert(resultado.errors)
        }
    });
}

export const obtenerAdminEstab = (id) => {
    let servicio = `${url}user/informationUser/${id}`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            document.querySelector("#nombre").value = resultado.result.Nombre;
            document.querySelector("#apellidos").value = resultado.result.Apellidos;
            document.querySelector("#email").value = resultado.result.Correo;
            document.querySelector("#telefono").value = resultado.result.Telefono;
        }
    });
}

export const actualizarAdminEstab = (parametros,idAdminEstab) => {
    console.log(parametros);
    let servicio = `${url}user/updateInformationUser/${idAdminEstab}`;
    let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Se actualizo de manera exitosa")
            self.location.reload();
        }else{
            alert(resultado.errors)
        }
    });
}

export const eliminarAdminEstab = (idAdminEstab) => {
    let servicio = `${url}user/deleteUser/${idAdminEstab}`;
    console.log(servicio)
    let api = new Api(servicio,"DELETE",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            self.location.reload();
        }
    })
}

export const llenarModalEliminarAdmiEstab = (id) => {
    document.querySelector("#modalLabelTitle").innerHTML = "Eliminar Administrador de Establecimiento";
    document.querySelector("#modalLabelBody").innerHTML = "Esta seguro de Eliminar el Administrador de Establecimiento?";
    document.querySelector("#modalActionButton").innerHTML = "Aceptar";
    document.querySelector("#modalActionButton").addEventListener("click", (e)=>{
        eliminarAdminEstab(id);
    }) 
}


export const initMap = (latitude,longitude) => {
    let map = null;
    // The location
    let centroMap = {lat: latitude, lng: longitude};
    // The map, centered at zone
    map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15, 
            center: centroMap,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    );
    
    const icon = {
        url: `${refUrlPag}img/pin-completo.png`, // url
        scaledSize: new google.maps.Size(60,60), // size
    };

    let marcador = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        icon: icon,
        draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend',  (e) => {
        document.querySelector("#Latitud").value = e.latLng.lat().toFixed(6);
        document.querySelector("#Longitud").value = e.latLng.lng().toFixed(6);
        map.panTo(e.latLng);
    });

    marcador.setMap(map);
}

export const llenarModalEliminarEstab = (id) => {
    document.querySelector("#modalLabelTitle").innerHTML = "Eliminar Establecimiento";
    document.querySelector("#modalLabelBody").innerHTML = "Esta seguro de Eliminar el Establecimiento?";
    document.querySelector("#modalActionButton").innerHTML = "Aceptar";
    document.querySelector("#modalActionButton").addEventListener("click", (e)=>{
        eliminarEstablecimiento(id);
    }) 
}

export const mostrarFormImgEstablecimiento = (id,tipo) => {
    document.querySelector("#tipoimg2").value = tipo;
    document.querySelector("#idElementoImg2").value = id;
    document.querySelector("#img").click();
}

// crud Establecimientos
export const listarEstablecimientos = () =>{
    let servicio = `${url}establishment/listAdmin`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        console.log(resultado);
        if (resultado.response) {
            let cuerpo = document.querySelector("#listaEstableciemientos");
            let data = resultado.result;
            window.llenarModalEliminarEstab = llenarModalEliminarEstab; //para poder llamar la funcion desde el cdocuemnto
            window.mostrarFormImgEstablecimiento = mostrarFormImgEstablecimiento;
            data.forEach(element => {
                let fila = document.createElement(`div`)
               
                fila.innerHTML = `
                <div class="col-xl-12 col-md-12 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        
                        <div style= "width:129px;">
                            <img src="${element.UrlFoto}" class="rounded" width="124" height="93">
                        </div>

                        <div class="col mr-1">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${element.Nombre}</div>
                            Telefono: ${element.Telefono}
                        </div>
                        
                      <div class="col-auto">

                      <!--<a href="agregar-producto.html?idEstab=${element.idEstablecimiento}" class="btn btn-success btn-circle">
                            <i class="fas fa-archive"></i>
                        </a>-->
                        
                        <button type="button" onclick="mostrarFormImgEstablecimiento(${element.idEstablecimiento},1)" class="btn btn-outline-primary"> <i class="fas fa-image"></i></button>
                        <a href="agregar-estab.html?id=${element.idEstablecimiento}" class="btn btn-info btn-circle">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" onclick = "llenarModalEliminarEstab(${element.idEstablecimiento})" id="eliminar${element.idEstablecimiento}" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-trash"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>`;
              cuerpo.appendChild(fila);
            });
            //   document.querySelector(`eliminar${element.idEstablecimiento}`).addEventListener('click',llenarModalEliminarEstab(element.idEstablecimiento));
        }else{

        }
    });
}

export const obtenerEstablecimiento = (id) => {
    let servicio = `${url}establishment/obtain/${id}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result;
            console.log(data);
            // informacion de establecimiento
            document.querySelector("#nombre").value = data.Nombre
            document.querySelector("#descripcion").value = data.Descripcion
            document.querySelector("#telefono").value = data.Telefono
            document.querySelector("#informacion").value = data.Direccion
            document.querySelector("#administrador").value = data.idPersona
            // document.querySelector("#categoria").value = data.idCategoria
            // let subcategorias = data.subcategorias;
            // // console.log(subcategorias);
            // listarSubCategorias(data.idCategoria,subcategorias);

            // document.querySelector("#Latitud").value = data.Latitud
            // document.querySelector("#Longitud").value = data.Longitud

            document.querySelector("#imgPerfil").src = data.UrlFoto;
        
            // background-image: url('img_girl.jpg');
            // initMap( parseFloat(data.Latitud),parseFloat(data.Longitud));
        }
    });
}

export const agregarEstablecimiento = (parametros) => {
    let servicio = `${url}establishment/add`;
    let api = new Api(servicio,"POST",parametros,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Establecimiento agregado de manera exitosa")
            document.querySelector("#idEstablecimiento").value = resultado.result;
            document.querySelector("#botonRegistrarEstab").disabled = true;
            self.location.reload();
        }else{
            alert("Intentalo de Nuevo")
        }
    });
}

export const actualizarEstablecimiento = (parametros,idEstablecimiento = 0)=>{
    console.log(parametros,idEstablecimiento);
    // establishment/update/5
    let servicio = `${url}establishment/update/${idEstablecimiento}`;
    let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Establecimiento agregado de manera exitosa")
            // document.querySelector("#idEstablecimiento").value = resultado.result;
            // document.querySelector("#botonRegistrarEstab").disabled = true;
            // document.querySelector("#divDireccion").style.display = "block";
            // self.location = "#divDireccion";
        }else{
            alert("Intentalo de Nuevo")
        }
    });
}
export const eliminarEstablecimiento = (idEstablecimiento) => {
    // alert("Se eliminarEstablecimiento")
    let servicio = `${url}establishment/delete/${idEstablecimiento}`;
    let api = new Api(servicio,"DELETE",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            self.location.reload();
        }
    })
}

// funciones de categorias y subcategorias
export const addCategoria = (id = 0) => {
    let parametros = {
        Descripcion:document.querySelector("#nombreCategoria").value
    }
    if (id == 0) {
        //    agregar
        let servicio = `${url}establishment/addCategoria`;
        let token = sessionStorage.getItem('tokenAdmin');
        let api = new Api(servicio,"POST",parametros,token);
        let res = api.call();
        res.then(resultado => {
            if (resultado.response) {
                 listarCategorias(true);
                 mostrarOcultar("fAddCategoria");
                 document.querySelector("#nombreCategoria").value = "";
            }
        })  
    }else{
         //    actualizar
         let servicio = `${url}establishment/updateCategoria/${id}`;
         let token = sessionStorage.getItem('tokenAdmin');
         let api = new Api(servicio,"PUT",parametros,token);
         let res = api.call();
         res.then(resultado => {
             if (resultado.response) {
                 listarCategorias(true);
                 mostrarOcultar("fAddCategoria");
                 document.querySelector("#nombreCategoria").value = "";
                 document.querySelector("#idCategoriaParaCategoria").value = 0;
             }
         })
    }
 }
 
 export const addSubcategoria = (idCategoria,idSubcategoria = 0) => {
     // let idCategoria  =document.querySelector("#idCategoriaParaSubcategoria").value;
     let parametros = {
         Descripcion:document.querySelector("#nombreSubcategoria").value,
         idCategoria: idCategoria
     }
     if (idSubcategoria == 0) {
         //    agregar
         let servicio = `${url}establishment/addSubcategoria`;
         let token = sessionStorage.getItem('tokenAdmin');
         let api = new Api(servicio,"POST",parametros,token);
         let res = api.call();
         res.then(resultado => {
             if (resultado.response) {
                 listarSubCategorias(idCategoria,null,true);
                 mostrarOcultar("fAddSubcategoria");
                 document.querySelector("#nombreSubcategoria").value = "";
             }else{
                 alert("Hay un error en los parametros, talves no selecciono una categoria")
             }
         })  
     }else{
          //    actualizar
          let servicio = `${url}establishment/updateSubcategoria/${idSubcategoria}`;
          let token = sessionStorage.getItem('tokenAdmin');
          let api = new Api(servicio,"PUT",parametros,token);
          let res = api.call();
          res.then(resultado => {
              if (resultado.response) {
                 listarSubCategorias(idCategoria,null,true);
                 mostrarOcultar("fAddSubcategoria");
                 document.querySelector("#nombreSubcategoria").value = "";
              }else{
                 alert("Hay un error en los parametros, talves no selecciono una categoria")
              }
          })
     }
 }

export const mostraFormUpdateCategoria = (idCategoria,Descripcion) => {
    let showForm = document.querySelector("#fAddCategoria").style.display;
    if (showForm == "none") {
        mostrarOcultar("fAddCategoria");
    }
    document.querySelector("#nombreCategoria").value = Descripcion;
    document.querySelector("#idCategoriaParaCategoria").value = idCategoria;
}

export const mostraFormUpdateSubcategoria = (idSubcategoria,Descripcion) => {
    let showForm = document.querySelector("#fAddSubcategoria").style.display;
    if (showForm == "none") {
        mostrarOcultar("fAddSubcategoria");
    }
    document.querySelector("#nombreSubcategoria").value = Descripcion
    document.querySelector("#idSubcategoria").value = idSubcategoria;
}

export const listarCategorias = (listaAdmin = false) => {
    let servicio = `${url}establishment/listCategories`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    let categorias = document.querySelector("#categoria");
    res.then(resultado => {
        let data = resultado.result;
        if (listaAdmin) {
            window.listarSubCategorias = listarSubCategorias;
            window.mostraFormUpdateCategoria = mostraFormUpdateCategoria;
            window.mostrarFormImgCaySubCat = mostrarFormImgCaySubCat;
            window.deleteCategoria = deleteCategoria;
            categorias.innerHTML = ``
            data.forEach(element => {
                console.log(data);
                categorias.innerHTML += `<li class="list-group-item d-flex justify-content-between "><div  align="left"><img src="${element.urlFoto}" width="30" height="30"><a href="#" onclick = "listarSubCategorias(${element.idCategoria},null,true)" >
                ${element.Descripcion}</a>
                </div>
                <div  align="rigth">
                <button type="button" onclick="mostrarFormImgCaySubCat(${element.idCategoria},2)" class="btn btn-outline-warning"> <i class="fas fa-image"></i></button>
                <button type="button" onclick="mostraFormUpdateCategoria(${element.idCategoria},'${element.Descripcion}')" class="btn btn-outline-info"> <i class="fas fa-edit"></i></button>
                <button type="button" onclick="deleteCategoria(${element.idCategoria})" class="btn btn-outline-danger"> <i class="fas fa-trash"></i></button></div>
              </li>`;
            })            
        } else {
            categorias.innerHTML += `<option disabled selected>Selecciona una opción</option>`;
            data.forEach(element => {
            categorias.innerHTML += `<option value="${element.idCategoria}" > ${element.Descripcion} </option>`;
        });        
        }
    });
}

export const deleteCategoria = (id) => {
    if (confirm("Esta seguro de elimianr esta Categoria")) {
         let servicio = `${url}establishment/deleteCategoria/${id}`;
         let token = sessionStorage.getItem('tokenAdmin');
         let api = new Api(servicio,"DELETE",null,token);
         let res = api.call();
         res.then(resultado => {
             if (resultado.response) {
                document.querySelector("#categoria").innerHTML = "";
                listarCategorias(true);
             }
         })
    }
}

export const deleteSubcategoria = (id) => {
    if (confirm("Esta seguro de elimianr esta Subcategoria")) {
         let idCategoria  =document.querySelector("#idCategoriaParaSubcategoria").value;
         let servicio = `${url}establishment/deleteSubcategoria/${id}`;
         let token = sessionStorage.getItem('tokenAdmin');
         let api = new Api(servicio,"DELETE",null,token);
         let res = api.call();
         res.then(resultado => {
             if (resultado.response) {
                 document.querySelector("#subCategoria").innerHTML = "";
                listarSubCategorias(idCategoria,null,true)
             }
         })
    }
}

export const mostrarFormImgCaySubCat = (id,tipo) => {
    document.querySelector("#tipoimg").value = tipo;
    document.querySelector("#idElementoImg").value = id;
    document.querySelector("#img").click();
}

// listar subcategorias
export const listarSubCategorias = (idCategoria,arraySelect = null,listaAdmin = false) => {
    let servicio = `${url}establishment/listSubcategories/${idCategoria}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        let data = resultado.result;
        let subcategorias = document.querySelector("#subCategoria");
            subcategorias.disabled = false;
        if (listaAdmin) {
            subcategorias.innerHTML = ``;
            if(data.length > 0){
                window.mostraFormUpdateSubcategoria = mostraFormUpdateSubcategoria;
                window.deleteSubcategoria = deleteSubcategoria;
                data.forEach(element => {
                    subcategorias.innerHTML += `<li class="list-group-item d-flex justify-content-between align-items-center"><div  align="left"><img src="${element.urlFoto}" width="30" height="30">
                    ${element.Descripcion}
                    </div>
                    <div  align="rigth">
                    <button onclick='mostrarFormImgCaySubCat(${element.idSubCategorias},3)' type="button" class="btn btn-outline-warning"> <i class="fas fa-image"></i></button>
                    <button onclick="mostraFormUpdateSubcategoria(${element.idSubCategorias},'${element.Descripcion}')" type="button" class="btn btn-outline-info"> <i class="fas fa-edit"></i></button>
                    <button onclick="deleteSubcategoria(${element.idSubCategorias})" type="button" class="btn btn-outline-danger"> <i class="fas fa-trash"></i></button></div>
                </li>`;
                })
                
            }else{
                subcategorias.innerHTML += `No hay elementos`;  
            }
            document.querySelector("#idCategoriaParaSubcategoria").value = idCategoria;
            document.querySelector("#idSubcategoria").value = 0;
            document.querySelector("#nombreSubcategoria").value = ""
        }else{
            if (arraySelect == null) {
                data.forEach(element => {
                    subcategorias.innerHTML += `<option value="${element.idSubCategorias}" > ${element.Descripcion} </option>`;
                });
            } else {
                let sel = ``;
                data.forEach(element => {
                    arraySelect.forEach(selected => {
                        if (selected.idSubCategorias == element.idSubCategorias) {
                            sel = `selected`;
                        }
                    });
                    subcategorias.innerHTML += `<option value="${element.idSubCategorias}" ${sel} > ${element.Descripcion} </option>`;
                    sel = '';
                });
                
            }
        }
    });
}

// cargar img
export const cargarImg = async (form = null, tipo = null, id = null) => {
    if (confirm('Quiere cargar este documento')) {
        let sw = ``;
        if (tipo != false) {
             sw = `${url}image/add/${tipo}/${id}`;
        }
       await fetch(sw, {
                method: 'POST',
                body: new FormData(form),
            })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res['response']) {
                    alert("Archivo cargado de manera correcta")
                    console.log(res);
                    if (tipo == 1) {
                        //document.querySelector("#imgPerfil").src = res.result["url"];
                        document.querySelector("#tipoimg2").value = 0;
                        document.querySelector("#idElementoImg2").value = 0;
                        listarEstablecimientos(true);
                    }else if(tipo == 2){
                        document.querySelector("#tipoimg").value = 0;
                        document.querySelector("#idElementoImg").value = 0;
                        listarCategorias(true);
                    }else if (tipo == 3){
                        let idCategoria  =document.querySelector("#idCategoriaParaSubcategoria").value;
                        listarSubCategorias(idCategoria,null,true);
                        document.querySelector("#tipoimg").value = 0;
                        document.querySelector("#idElementoImg").value = 0; 
                    };
                } else {
                    alert(res['errors']);
                    console.log(res);
                }
            });
    }
}


// crud admin establecimiento
export const listarMembresias = (forselect = false) => {
    let servicio = `${url}membership/list`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result.Data;
            if (!forselect) {
                let cuerpo = document.querySelector("#listaMembresia");
                window.llenarModalEliminarMembresia = llenarModalEliminarMembresia;
                data.forEach(element => {
                    let fila = document.createElement(`div`)
                    fila.innerHTML = `
                    <div class="col-xl-12 col-md-12 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${element.Usuario}</div>
                            <div style="font-size:1rem" class="mb-0 text-success">FechaUltimoPago: ${element.FechaUltimoPago}</div>
                            <div style="font-size:1rem" class="mb-0 text-danger">FechaExpiracion: ${element.FechaExpiracion}</div>
                        </div>
                        <div class="col-auto">
                            <a href="agregar-membresia.html?id=${element.idMembresia}" class="btn btn-info btn-circle">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" onclick="llenarModalEliminarMembresia(${element.idMembresia})" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    </div>`;
                    cuerpo.appendChild(fila);
                    document.querySelector(`#eliminar${element.idMembresia}`);

                });
            }else{
                let lista = document.querySelector("#usuario");
                    lista.innerHTML += `<option value = "0"> Sin seleccion </option>`;
                data.forEach(element => {
                        lista.innerHTML += `
                        <option value="${element.idPersona}">${element.Nombre} ${element.Apellidos} - ${element.Telefono} </option>
                    `;
                    
                });
            }
            
            
        }else{

        }
        
    });
}


export const agregarMembresia = (parametros) => {
    let servicio = `${url}membership/add`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"POST",parametros,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Membresia agregado de manera exitosa")
            self.location.reload();
        }else{
            alert(resultado.errors)
        }
    });
}

// list tipo user membresias
export const listarUserMembresia = (forselect = false) => {
    let servicio = `${url}user/toList/1`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result.Data;

                let lista = document.querySelector("#administrador");
                    lista.innerHTML += `<option value = "0"> Sin seleccion </option>`;
                data.forEach(element => {
                        lista.innerHTML += `
                        <option value="${element.idPersona}">${element.Nombre} ${element.Apellidos} - ${element.Telefono} </option>
                    `;
                    
                });
            }
        
    });
}

export const obtenerMembresia = (id) => {
    let servicio = `${url}membership/detail/${id}`;
    let token = sessionStorage.getItem('tokenAdmin');
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result;
            console.log(data)
            document.querySelector("#administrador").value = data.idPersona;
            document.querySelector("#fechaultimopago").value = data.FechaUltimoPago;
            document.querySelector("#fechaexpiracion").value = data.FechaExpiracion;
            document.querySelector("#divFechas").style.display = "block";
        }
    });
}

export const actualizarMembresia = (parametros,idMembresia) => {
    console.log(parametros);
    let servicio = `${url}membership/update/${idMembresia}`;
    let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            alert("Se actualizo de manera exitosa")
            self.location.reload();
        }else{
            alert(resultado.errors)
        }
    });
}

export const eliminarMembresia = (idMembresia) => {
    let servicio = `${url}membership/delete/${idMembresia}`;
    console.log(servicio)
    let api = new Api(servicio,"DELETE",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            self.location.reload();
        }
    })
}

export const llenarModalEliminarMembresia = (id) => {
    document.querySelector("#modalLabelTitle").innerHTML = "Eliminar Membresia";
    document.querySelector("#modalLabelBody").innerHTML = "Esta seguro de Eliminar esta Membresia";
    document.querySelector("#modalActionButton").innerHTML = "Aceptar";
    document.querySelector("#modalActionButton").addEventListener("click", (e)=>{
        eliminarMembresia(id);
    }) 
}