import {auth,listarEstablecimientos,cargarImg} from './app.js';
import {cargarGeneric} from './generic/generics.js';

// validar token
auth();

cargarGeneric();
// listar Establecientos
listarEstablecimientos();

const img = document.querySelector("#img");

img.addEventListener("change",(e)=>{
    let formImg = document.querySelector("#cargarImg");
    let tipo = document.querySelector("#tipoimg2").value;
    let idElemento = document.querySelector("#idElementoImg2").value;
    console.log(formImg,tipo,idElemento);
    cargarImg(formImg,tipo,idElemento);
})