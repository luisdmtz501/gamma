import {auth,listarMembresias} from './app.js';
import {cargarGeneric} from './generic/generics.js';
// validar token
auth()
// listar administradores de Establecimientos
listarMembresias();

cargarGeneric();