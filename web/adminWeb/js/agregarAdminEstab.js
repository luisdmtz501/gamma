import {auth,obtenerAdminEstab,actualizarAdminEstab,agregarAdminEstab} from './app.js';
import {cargarGeneric} from './generic/generics.js';

// validar token
auth()
cargarGeneric();

//saber si es un update
// valores de la url
let sear = new URLSearchParams(location.search);
let idAdminEstab = sear.get('id');

if (idAdminEstab != null) {
    obtenerAdminEstab(idAdminEstab);
}
// 
const formRegistrarAE = document.querySelector("#formRegistrarAE");

formRegistrarAE.addEventListener('submit', (e) => {
    e.preventDefault();
    let password = document.querySelector("#password").value;
    if (password == '') {
        password = null
    }
    let parametros =  {
        Nombre:document.querySelector("#nombre").value,
        Apellidos:document.querySelector("#apellidos").value,
        Correo:document.querySelector("#email").value,
        Password:password,
        Telefono:document.querySelector("#telefono").value,
        idTipoPersona:2,
        Activo:1,
    }
    if (idAdminEstab != null) {
        actualizarAdminEstab(parametros,idAdminEstab);
    }else{
        agregarAdminEstab(parametros);
    }
    
});