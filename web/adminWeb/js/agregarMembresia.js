import {auth,obtenerMembresia,actualizarMembresia,agregarMembresia,listarUserMembresia} from './app.js';
import {cargarGeneric} from './generic/generics.js';

// validar token
auth()
cargarGeneric();

//saber si es un update
// valores de la url
let sear = new URLSearchParams(location.search);
let idMembresia = sear.get('id');

listarUserMembresia(true);

if (idMembresia != null) {
    obtenerMembresia(idMembresia);
}
// 
const formRegistrarAE = document.querySelector("#formRegistrarAE");


formRegistrarAE.addEventListener('submit', (e) => {
    e.preventDefault();

    let parametros =  {
        FechaUltimoPago:document.querySelector("#fechaultimopago").value,
        FechaExpiracion:document.querySelector("#fechaexpiracion").value,
        idPersona:document.querySelector("#administrador").value,
    }
    if (idMembresia != null) {
        actualizarMembresia(parametros,idMembresia);
    }else{
        agregarMembresia(parametros);
    }
    
});