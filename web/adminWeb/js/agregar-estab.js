import {auth,obtenerEstablecimiento,agregarEstablecimiento,listarAdminEstab,actualizarEstablecimiento,cargarImg,/*initMap*/} from './app.js';
import {cargarGeneric} from './generic/generics.js';
import {refUrlPag} from '../../js/config/config.js'

// validar token
auth()
cargarGeneric();
// listas
let sear = new URLSearchParams(location.search);
let idEstab = sear.get('id');

// listarCategorias();

listarAdminEstab(true);
let latMapa = parseFloat(20.174633); 
let longMap = parseFloat(-98.050865);
if (idEstab != null) {
    
    obtenerEstablecimiento(idEstab);
    document.querySelector("#idEstablecimiento").value = idEstab;
}
//eventos de la pagina 
const botonRegistrarEstab = document.querySelector("#botonRegistrarEstab");
const categoria = document.querySelector("#categoria");
const inputCargarPerfil = document.querySelector("#imaperfil");

// window.addEventListener("load",()=>{
//     initMap(latMapa, longMap);
// })
// // 
// categoria.addEventListener('change',()=>listarSubCategorias(categoria.value))

// boton resgistro de Establecimiento
botonRegistrarEstab.addEventListener('click',(e)=>{
    e.preventDefault();
    // mapeo de multiselect
    // const selected = document.querySelectorAll('#subCategoria option:checked');
    // const values = Array.from(selected).map(el => el.value);
    // let subcategorias = ``;
    // let i = 0;
    // // formar cadena de subcategorias
    // values.forEach(element => {
    //     if (i != 0) {
    //         subcategorias+=`-${element}`;
    //     }else{
    //         subcategorias+=`${element}`
    //     }
    //     i++
    // });
    
    let parametros = {
        "Nombre":document.querySelector("#nombre").value,
        "Descripcion":document.querySelector("#descripcion").value,
        "Direccion":document.querySelector("#informacion").value,
        "Telefono":document.querySelector("#telefono").value,
        // "idCategoria":document.querySelector("#categoria").value,
        // "subcategorias":subcategorias,
        "idPersona": document.querySelector("#administrador").value,
        // "Latitud":document.querySelector("#Latitud").value,
        // "Longitud":document.querySelector("#Longitud").value,
        "idStatusEstablecimiento":1
    }
    if (idEstab != null) {
        actualizarEstablecimiento(parametros,idEstab)
    }else{
        agregarEstablecimiento(parametros)
    }
});
//mapa para escojer lon lat


// window.addEventListener("load",()=>{
//     initMap(latMapa, longMap);
// })


inputCargarPerfil.addEventListener('change',()=>{
    let form = document.querySelector("#formFotoPerfilEstab");
    cargarImg(form,1,document.querySelector("#idEstablecimiento").value);    
});
