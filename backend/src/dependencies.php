<?php

$container = $app->getContainer();


// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        'auth' => new App\Model\AuthModel($c->db),
        'user' => new App\Model\UserModel($c->db),
        'promotion' => new App\Model\PromotionModel($c->db),
        'scan' => new App\Model\ScanModel($c->db),
        'establishment' => new App\Model\EstablishmentModel($c->db),
        'image' => new App\Model\ImageModel($c->db),
        'membership' => new App\Model\MembershipModel($c->db),
        'places' => new App\Model\PlacesModel($c->db),
        'calificacion' => new App\Model\CalificacionModel($c->db),
        'eventos' => new App\Model\EventosModel($c->db),
        'delete_user' => new App\Model\DeleteModel($c->db),
        'centros_autorizados' => new App\Model\CentrosAutorizados($c->db),
        'verify_membership' => new App\Model\VerifyMembership($c->db)
    ];
};
