<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\RestApi,
    App\Lib\RestApiMethod,
    App\Lib\Push;

class ScanModel{

    private $db;
    private $tbPromocion = 'promocion';
    private $tbUser = 'persona';
    private $tbEstablecimiento = 'establecimiento';
    private $tbPromoUsada = 'promousada';
    private $tbMembresia = 'membresia';
    private $tbPeriodo = 'periodo';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }
    // SI FUNCIONA
    public function addScan($idPersona,$idPromocion){

        $usuario = $this->db->from($this->tbUser)
            ->where('id', $idPersona)
            ->where('status','activo')
            ->fetch();

            if ($usuario != false) {
                $suscripcion = $this->db->from($this->tbMembresia)
                    ->where("idPersona",$idPersona)
                    ->orderBy("idMembresia DESC")
                    ->limit(1)
                    ->fetch();

                if ($suscripcion != false){
                    
                    if (date('Y-m-d H:i:s') < $suscripcion->fechaExpiracion) {
                        $periodo = $this->db->from($this->tbPeriodo)
                        ->where("status = 1")
                        ->fetch();

                    if ($periodo != false) {
                        $promocion = $this->db->from($this->tbPromocion)
                            ->where('id',$idPromocion)
                            ->fetch();

                        if ($promocion != false) {
                                $buscar_periodo = $promocion->idPeriodo;
                                $fecha  = date('Y-m-d');
                                switch ($buscar_periodo) {
                                    case 1:#Libre
                                        $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                           ->execute();
                                               $this->response->result = $result;
                                        return $this->response->SetResponse(true, "Tu promocion fue un ¡Exito!");
                                        break;

                                    case 2:#Dia
                                        $buscarUso = $this->db->from($this->tbPromoUsada)
                                                             ->where(" Fecha between :fechaini and :fechafin and idPromocion = :idpromocion",
                                                              [
                                                                ':fechaini' => "$fecha 00:00:00",
                                                                ':fechafin' => "$fecha 23:59:59",
                                                                ':idpromocion' => $idPromocion
                                                              ])
                                                              ->fetch();

                                        if ($buscarUso != false) {
                                                   $this->response->result = $buscarUso;
                                                   $this->response->errors = 'Esta promo ya fue usada en el periodo permitido de un dia';
                                            return $this->response->SetResponse(false);
                                        }
                                        
                                        $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                           ->execute();

                                               $this->response->result = $result;
                                        return $this->response->SetResponse(true, "Tu promocion de hoy fue un ¡Exito!");
                                        break;

                                    case 3:#Semana
                                        $fechaUso = $this->db->from($this->tbPromoUsada)
                                                             ->select(null)
                                                             ->select("Fecha")
                                                             ->where("idPersona = '$idPersona' AND idPromocion = '$idPromocion'")
                                                             ->orderBy("Fecha DESC")
                                                             ->fetch();
                                        if($fechaUso){
                                            $fechaSplit = explode(" ", $fechaUso->Fecha);
                                            $fechaIni = date("Y-m-d",strtotime($fechaSplit[0]));
                                            $fechaFin = date("Y-m-d",strtotime($fechaIni."+ 7 days"));

                                            if($fecha >= $fechaIni && $fecha < $fechaFin){
                                                $this->response->result = [];
                                                $this->response->errors = 'Esta promo ya fue usada en el periodo permitido de una semana';
                                                return $this->response->SetResponse(false);
                                            }else{
                                                $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                               ->execute();
                                                $this->response->result = $result;
                                                return $this->response->SetResponse(true, "Tu promocion semanal fue un ¡Exito!");
                                            }
                                        }else{
                                            $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                               ->execute();
                                            $this->response->result = $result;
                                            return $this->response->SetResponse(true, "Tu promocion semanal fue un ¡Exito!");
                                        }
                                        break;

                                    case 4:#Mes
                                        $fechaUso = $this->db->from($this->tbPromoUsada)
                                        ->select(null)
                                        ->select("Fecha")
                                        ->where("idPersona = '$idPersona' AND idPromocion = '$idPromocion'")
                                        ->orderBy("Fecha DESC")
                                        ->fetch();

                                        if($fechaUso){
                                            $fecha2 = date('Y-m');
                                            $fechaSplit = explode(" ", $fechaUso->Fecha);

                                            $char = "-"; 
                                            $lastPosition = strrpos($fechaSplit[0], $char); 
                                            $fechaSplit = substr($fechaSplit[0], 0, $lastPosition);
                                            if(strtotime($fecha2) != strtotime($fechaSplit)){
                                                $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                               ->execute();
                                                $this->response->result = $suscripcion->fechaUltimoPago;
                                                return $this->response->SetResponse(true, "Tu promocion mensual fue un ¡Exito!");
                                            }else{
                                                $this->response->result = $suscripcion;
                                                $this->response->errors = 'Esta promo ya fue usada en el periodo permitido de un mes';
                                                return $this->response->SetResponse(false);
                                            }
                                        }else{
                                            $result = $this->db->insertInto($this->tbPromoUsada, ['idPersona' => $idPersona, 'idPromocion' => $idPromocion, 'Fecha' => date('Y-m-d H:i:s')])
                                                               ->execute();
                                            $this->response->result = $suscripcion->fechaUltimoPago;
                                            return $this->response->SetResponse(true, "Tu promocion mensual fue un ¡Exito!");
                                        }
                                        break;
                                    }
                         
                                } else {
                                   $this->response->errors='Esta promocion no esta disponible';
                            return $this->response->SetResponse(false);
                            }  
                        } else {
                              $this->response->errors='El periodo de promocion se encuentra inactivo';
                       return $this->response->SetResponse(false);
                    } 
                    }else{
                               $this->response->errors= 'Tu suscripcion ya expiró, activa una nueva';
                        return $this->response->SetResponse(false);
                    }
                 } else {
                     $this->response->errors='No hay una suscripcion activa';
              return $this->response->SetResponse(false);
           }
         } else {
              $this->response->errors='Usuario inactivo, no se puede validar tu promocion';
      return  $this->response->SetResponse(false);
      }
   }
   //SI funciona
    public function validateMembership($id){
        $usuario = $this->db->from($this->tbUser)
                            ->where('id', $id)
                            ->where('status','activo')
                            ->fetch();

        if ($usuario != false) {
            $fecha = date('Y-m-d H:i:s');

            $suscripcion = $this->db->from($this->tbMembresia)
                                    ->where("idPersona = '$usuario->id'")
                                    ->orderBy("idMembresia DESC")
                                    ->limit(1)
                                    ->fetch();
        
            if ($suscripcion != false) {
                if (strtotime($fecha) < strtotime($suscripcion->fechaExpiracion)) {
                    
                            $this->response->result=$usuario;
                    return  $this->response->SetResponse(true);
                }else{
                            $this->response->errors='qr no valido';
                    return  $this->response->SetResponse(false);
                }
            }else{
                        $this->response->errors='No hay una suscripcion activa';
                return  $this->response->SetResponse(false);
            }
                    
        } else {
                    $this->response->errors='Usuario inactivo, no se puede validar tu promocion';
            return  $this->response->SetResponse(false);
        }
   }
}