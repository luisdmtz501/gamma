<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class UserModel 
{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	private $tbMembresia = 'membresia';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	#-----------------------------------------SERVICIOS----------------------------------------------------------------
	#servicio de Registro
	#Valida que no halla algun otro usuario con la misma informacion 	
	//si funcionar
	public function registerUser($data){
		if(filter_var($data['correo'],FILTER_VALIDATE_EMAIL)) {
			if(strlen($data['password']) >= 2){
				#data(contiene el body que son los datos que se van a insertar en la tabla)
				$email = $this->db->from($this->tablePerson)
								->where('correo', $data['correo'])
								//->where('idTipoPersona', $data['idTipoPersona'])
								->where('status','activo')
								->fetch(); #fetch cuando solo es uno solo

				if($email!=false){
						$this->response->errors = "Ya existe un usuario con esa información";
					return $this->response->SetResponse(false);
				}else{
					$array = array('password' => Cifrado::Sha512($data['password'])); #para encriptar la contraseña
					$new_data = array_merge($data, $array); #array_merge:vincula n numero de arreglos para hacerlo uno solo

					$register = $this->db->insertInto($this->tablePerson, $new_data)
										->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
					#falta mostrar la fecha actual del registro
				}
			}else{
					   $this->response->errors='Apellido no valido.';
				return $this->response->SetResponse(false);
			}
		}else{
				   $this->response->errors='Correo no valido.';
	        return $this->response->SetResponse(false); 
		}
	}
	//si funciona
	public function informationUser($id){
		$obtener = $this->db->from($this->tablePerson)
							->select(null)
						    ->select('Nombre, Apellidos, Correo, Password, urlImg, Telefono')
						    ->where('id', $id)
						    ->fetch();

				   $this->response->result = $obtener;	
			return $this->response->SetResponse(true);
	}
	//si funciona
	public function updateInformationUser($data,$idPersona){
	    $buscar = $this->db->from($this->tablePerson)
	                     ->where('id', $idPersona)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='Usuario no existe.';
	      return $this->response->SetResponse(false);
	    }else{
			if (isset($data['Password'])) {
				$data['Password'] = Cifrado::Sha512($data['Password']);
			}else{
				unset($data['Password']);
			}
	    	 $actualizar = $this->db->update($this->tablePerson, $data) 
	                                ->where('id',$idPersona)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }
	//SI FUNCIONA
	public function deleteUser($id){
		$this->db->update($this->tablePerson)
				 ->set('status','inactivo') #set actualiza la columna indicada por el valor indicado
				 ->where('id', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
	}
	#Actualizar contraseña
	//si funciona 
    public function updatePassword($user, $password){
    	$buscar_usuario_registrado = $this->db->from($this->tablePerson)
    		->select('password')
	        ->where('correo', $user)
	        ->fetch();

	    if ($buscar_usuario_registrado) {
			$registredPassword = $buscar_usuario_registrado->password;
	    	$password = Cifrado::Sha512($password);

			if($registredPassword != $password){
				$actualizar = $this->db->update($this->tablePerson)
					->set('password', $password)
					->where('correo', $user)
					->execute();
	
					   $this->response->result = $actualizar;
				return $this->response->SetResponse(true, 'Actualización correcta.');
			} else {
						$this->response->errors = "No puedes registar la misma contraseña.";
		 		return $this->response->SetResponse(false);
			}
	    } else {
	    		   $this->response->errors = "El usuario al que hace referencia no existe.";
	    	return $this->response->SetResponse(false);
	    }
    }
	//pendiente
	public function toList($TipoPersona){
		if ($TipoPersona == 2) {
			$data = $this->db->from($this->tablePerson)
    					 ->select(null)
						 ->select('persona.idPersona, persona.Nombre, Apellidos, Correo, Password, FotoPerfil, persona.Telefono, idTipoPersona, idStatusPersona, IFNULL(establecimiento.Nombre, "Sin detalle") AS NombreEstablecimiento')
						 ->leftJoin('establecimiento on establecimiento.idPersona = persona.idPersona')
						 ->where('idTipoPersona',$TipoPersona)
						 ->where('idStatusPersona != 2')
						//  ->where('establecimiento.idStatusEstablecimiento != 2')
						 ->orderBy('idPersona DESC') #ASC
						 ->groupBy('persona.idPersona')
    					 ->fetchAll();
		} elseif ($TipoPersona == 1) {
			$data = $this->db->from($this->tablePerson)
    					 ->select(null)
    					 ->select('persona.idPersona, Nombre, Apellidos, Correo, Password, FotoPerfil, Telefono, idTipoPersona, idStatusPersona')
						 ->where('idTipoPersona',$TipoPersona)
						 ->where('idStatusPersona != 2')
						 ->orderBy('idPersona DESC') #ASC
    					 ->fetchAll();
		}

    	$total = $this->db->from($this->tablePerson)
						  ->select('COUNT(*) total')
						  ->where('idTipoPersona',$TipoPersona)
						  ->where('idStatusPersona != 2')
    					  ->fetch()
    					  ->total;

    			$this->response->result = ['Data' => $data, 'Total' => $total];
    	return $this->response->SetResponse(true);
    }

	public function toListMembership($idPers){
		$proxPago = $this->db->from($this->tbMembresia)
    					 ->select(null)
    					 ->select('fechaExpiracion,status')
						 ->where('idPersona',$idPers)
						 ->where('status','activo')
    					 ->fetch();
		
		if($proxPago!=false)
		{
			$data=[
				"Plan actual"=>"mensual",
				"Precio"=>"$29.00",
				"Metodo de Pago"=>"Efectivo",
				"Proximo Pago"=> date('d/m/Y', strtotime($proxPago->fechaExpiracion))
			];	


			$this->response->result = $data;
    		return $this->response->SetResponse(true);
		}else{
			$this->response->result = "Su menbresia ha expirado el dia " . date('d/m/Y', strtotime($proxPago->fechaExpiracion));
    		return $this->response->SetResponse(false);
		}
		
	}

	
  }
  
 ?>