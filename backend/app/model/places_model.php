<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class PlacesModel 
{
	private $db;
	private $response;
	private $tablePlace = 'lugares';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	//Agregar lugar
	public function agregarLugar($data){
        $register = $this->db->insertInto($this->tablePlace, $data)
							 ->execute();
							 
        
		       $this->response->result = $register;
		return $this->response->SetResponse(true, "Registro exitoso");
	}

	//Consultar lugar por id
	public function getBy($id){
		$obtener = $this->db->from($this->tablePlace)
							->select('nombre')
						    ->select('descripcion')
						    ->where('idlugares', $id)
						    ->fetch();
				
				if	($obtener != false) {
					$this->response->result=$obtener;
			 return $this->response->SetResponse(true);
		 	 }else{
					$this->response->errors='Error, este lugar no existe';
			 return $this->response->SetResponse(false);
		 }
	}
	
	//Listar lugares
    public function toList($limit = 10,$offset = 0){
		$listar = $this->db->from($this->tablePlace)
		->select(null)
		->select('idlugares')
		->select('nombre')
		->select('descripcion')
		->orderBy('idlugares DESC') #ASC
		->fetchAll();

		if	($listar !=false)	{
		$this->response->result=['Data' => $listar];
		return $this->response->SetResponse(true);
		}else{
		$this->response->errors='No existen lugares';
		return $this->response->SetResponse(false);
		}
    }
	
	//Actualizar lugar
	public function updateLugar($data, $id){
		$actualizar = $this->db->update($this->tablePlace, $data)
					->where('idlugares',$id)
					->execute();
		if ($actualizar != false) {
			$this->response->result = $actualizar;
		return $this->response->SetResponse(true,'Lugar actualizado con exito');
		}else{
				$this->response->errors='Error al actualizar el lugar';
		return  $this->response->SetResponse(false);
		}
	}
	
	//Eliminar lugar
   	public function delete($id){
		$eliminar = $this->db->deleteFrom($this->tablePlace)
				 ->where('idlugares',$id)
				 ->execute();

		if	($eliminar != false) {
				   $this->response->result=$eliminar;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				   $this->response->errors='¡Error, al eliminar el lugar, verifique nuevamente!';
			return $this->response->SetResponse(false);
        }
	}
}