<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Configuracion,
	App\Lib\Codigos;

class ImageModel{
	private $db;
	private $response;
	private $tableEstablishmnet = 'establecimiento';
	private $tableCategoria = 'categoria';
	private $tableSubcategoria = 'subcategorias';
	private $tbPersona='persona';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}
	
	public function add($file,$tipo,$id){
		$conf = Configuracion::getUrl();
		$tabla = "";
		$idTabla = "";
		$dirHost = $conf['host'];
		$dirServer = $conf['servidor'];
		
		if ($file['img']['error'] > 0 or $file['img']['tmp_name']=="") {
				   $this->response->result = $file;
				   $this->response->errors = 'Tenemos problemas al cargar el archivo';
	 		return $this->response->SetResponse(false);
		}else{
			// 
			$tringImg = $file['img']['name'];
			$arraynameImg = explode(".",$tringImg);
			$lar = count($arraynameImg) - 1;
			$tipe = $arraynameImg[$lar];
			// 
			$codigo = Codigos::Codigo(10);
			$nameImg = "$codigo"."_"."$id.$tipe";
			$dirServerForAnte = $dirServer;
			switch ($tipo) {
				case 1:#img establecimiento
					$tabla = $this->tableEstablishmnet;
					$idTabla = "id";
					$dirHost .= "img/logos/$nameImg";
					$dirServer .= "img/logos/$nameImg";
					$dirServerForAnte .= "img/logos/";
					$campo = "urlImg";
					break;
				case 2:#img categorias
					$tabla = $this->tableCategoria;
					$idTabla = "idCategoria";
					$dirHost .= "img/categorias/$nameImg";
					$dirServer .= "img/categorias/$nameImg";
					$dirServerForAnte .= "img/categorias/";
					$campo = "urlFoto";
					break;
				case 3:#img subcategorias
					$tabla = $this->tableSubcategoria;
					$idTabla = "idSubCategorias";
					$dirHost .= "img/subcategorias/$nameImg";
					$dirServer .= "img/subcategorias/$nameImg";
					$dirServerForAnte .= "img/subcategorias/";
					$campo = "urlFoto";
					break;
				case 4:#img Perfil
					$tabla = $this->tbPersona;
					$idTabla = "id";
					$dirHost .= "img/perfil/$nameImg";
					$dirServer .= "img/perfil/$nameImg";
					$dirServerForAnte .= "img/perfil/";
					$campo = "urlImg";
					break;
				case 5:#img establecimientoPerfil
					$tabla = $this->tableEstablishmnet;
					$idTabla = "id";
					$dirHost .= "img/perfil/$nameImg";
					$dirServer .= "img/perfil/$nameImg";
					$dirServerForAnte .= "img/perfil/";
					$campo = "urlImgPerfil";
					break;
			}

			try{
				$buscarImgAnterior = $this->db->from($tabla)
												->select(null)
												->select("$campo As Url")
												->where("$idTabla", $id)
												->fetch()
												->Url;
				
				if($buscarImgAnterior!= null){
					$urlAnte = $buscarImgAnterior;
					$arraynameImg = explode("/",$urlAnte);
					$lar = count($arraynameImg) - 1;
					$nameAnt = $arraynameImg[$lar];
					$urlBorrar = "$dirServerForAnte$nameAnt";
					unlink("$urlBorrar");
				}
				if($mover = move_uploaded_file($file['img']['tmp_name'], $dirServer)){
					$insertar = $this->db->update($tabla)
										->set("$campo", $dirHost)
										->where("$idTabla", $id)
										->execute();

					  	   $this->response->result = [
								 "tipo"=>$tabla,
								 "url"=>$dirHost,
								 "status"=>$mover
								];
					return $this->response->SetResponse(true, "Se ha agregado la foto");
				}
			}catch(\Exception $e){
					$this->response->errors='Error encontrado: '.$e->getMessage()."\n";
			return $this->response->SetResponse(false);
			}
		}
	}

	public function updateUri($tipo){
		$conf = Configuracion::getUrl();
		$tabla = "";
		$idTabla = "";
		$dirHost = $conf['host'];
			// $dirServer = $conf['servidor'];
		switch ($tipo) {
			case 1:
				$tabla = $this->tableEstablishmnet;
				$idTabla = "idEstablecimiento";
				$campo = "UrlFoto";
				$dirHost .= "img/establecimientos/";
				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);
				
				break;
			case 2:
				$tabla = $this->tableCategoria;
				$idTabla = "idCategoria";
				$dirHost .= "img/categorias/";
				$campo = "urlFoto";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 3:
				$tabla = $this->tableSubcategoria;
				$idTabla = "idSubCategorias";
				$dirHost .= "img/subcategorias/";
				$campo = "urlFoto";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
		}
	}

	public function updateInBase($tabla,$campo,$idTabla,$dirHost){
		
		$td = $this->db->from($tabla)
								->select(null)
								->select("$idTabla,$campo")
								->where("$campo != '' ")
								->fetchAll();
		foreach ($td as $value) {
			
			$valueAnt = $value->$campo;
			$imagenExplode = explode('/', $value->$campo);
			$nameImg = $imagenExplode[count($imagenExplode) - 1];
			// "$nameImg";
			$dir_host = "$dirHost$nameImg";
			$data = [
				$campo => $dir_host
			];
			$actualizar = $this->db->update($tabla,$data)
									->where($idTabla,$value->$idTabla)
									->execute();
			$result[] = [
				"id" => $value->$idTabla,
				"ante"=>$valueAnt,
				"host"=>$dir_host,
				"data" => $data
			  ];
		}
		return $result;
	}
}