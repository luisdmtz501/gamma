<?php 
namespace App\Model;

use App\Lib\response;


class EstablishmentModel
{
	private $db;
	private $response;
	private $tableEstablishment = 'establecimiento';
	private $tableHasSubTypeEstablishment = 'categoria_has_subcategorias';
	private $tableSubTypeEstablishment = 'subcategorias';
	private $tableTypeEstablishment = 'categoria';
	private $tablePromos = 'promocion';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#Servicios
	//funciona bien, solo que la categori no se agrega en la tabla categorias	
	public function addEstablishment($data){
			$subcategorias = false; 
			// existe subcategorias
			if (isset($data['subcategorias'])) {
				$subcategorias = $data['subcategorias'];
				unset($data['subcategorias']);
			}
			// idAdmin 0 id de persona
			if ($data['idPersona'] == 0) {
				unset($data['idPersona']);
			}
			// alta de establecimiento
			$register = $this->db->insertInto($this->tableEstablishment, $data)
								 ->execute();
								 
			if ($register > 0 ) {
				// alta de subcategorias
				if ($subcategorias != false) {
					$arraySubcategorias = explode("-",$subcategorias);
					foreach ($arraySubcategorias as $value) {
						$dataSub = array(
							"idEstablecimiento"=>$register,
							"idSubCategorias"=>$value
						);
						$insertarSubcategorias = $this->db->insertInto($this->tableHasSubTypeEstablishment,$dataSub)
													      ->execute();
					}
				}
				if($register){
		    	    $this->response->result = $register;
		    		return $this->response->SetResponse(true, "Registro exitoso");
		    	}else{ 
		          	$this->response->errors = "Error al registrar";
		    		return $this->response->SetResponse(false, "Error al registrar");
		    	}
			}
	}
// duda respecto a la direccion
//funciona bien
	public function obtain($id){
		$obtener = $this->db->from($this->tableEstablishment)
							->select('establecimiento.id, direccion, latitud, longitud')
							->where('establecimiento.id', $id)
							->fetch();
		
		$subcategorias = $this->db->from($this->tableEstablishment)
								  ->select(null)
								  ->select('subcategorias.descripcion, subcategorias.id')
								  ->leftJoin('categoria_has_subcategorias AS bridge on establecimiento.id = bridge.idEstablecimiento')
								  ->leftJoin('subcategorias on subcategorias.id = bridge.idSubCategorias')
								  ->where('establecimiento.id',$id)
								  ->fetchAll();
		$obtener = get_object_vars($obtener); //convertir objeto en arreglo
		//$subcategorias = get_object_vars($subcategorias); //convertir objeto en arreglo
		// var_dump($obtener);
		// var_dump($subcategorias);
		$sub = array(
			'subcategorias'=>$subcategorias
		);
		
		$new_data = array_merge($obtener, $sub);
		$new_data = array_merge($new_data);
				   $this->response->result = $new_data;		
			return $this->response->SetResponse(true);
	}
	// TODO tiempo estimado, costo de envio, numero de pedidos que ha realizado el establecimiento,
	//pediente por resolver 
	public function toListForSubcategories($idSubcategoria){
		$data = $this->db->from($this->tableEstablishment)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,
						 CONCAT(direccion, IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,categoria.Descripcion AS Categoria, establecimiento.urlFoto as UrlFoto')
						 ->leftJoin('Direccion on Direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('categoria on categoria.idCategoria = establecimiento.idCategoria')
						 ->leftJoin('categorias_has_subcategorias as bridge on establecimiento.idEstablecimiento = bridge.idEstablecimiento')
						 ->leftJoin('subcategorias on subcategorias.idSubCategorias = bridge.idSubCategorias')
						 ->where('subcategorias.idSubCategorias',$idSubcategoria)
						 ->where('idStatusEstablecimiento', 1)
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
    					 ->fetchAll();
		// echo $data->getQuery() . "\n";
		// var_dump($data);
		// die;

		$data = $this->agregarSubcaterias($data);
		
    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
	}
	//pediente por resolver
	public function toListForCategories($idCategoria){
		$data = $this->db->from($this->tableEstablishment)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS Nombre,urlFoto AS UrlImag, 
						 CONCAT(Direccion, IFNULL(Direccion.Latitud,0) AS Latitud,IFNULL(Direccion.Longitud, 0) AS Longitud,categoria.Descripcion AS categoria, establecimiento.urlFoto as UrlFoto')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('categoria on categoria.idCategoria = establecimiento.idCategoria')
						 ->leftJoin('categoria_has_subcategorias as bridge on establecimiento.idEstablecimiento = bridge.idEstablecimiento')
						 ->leftJoin('subcategorias on subcategorias.idSubCategorias = bridge.idSubCategorias')
						 ->where('establecimiento.idCategoria',$idCategoria)
						 ->where('idStatusEstablecimiento', 1)
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
						 ->fetchAll();

		$data = $this->agregarSubcaterias($data);
		

			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}
	//si funciona
	public function update($id,$data){
		$obtener = $this->db->from($this->tableEstablishment)
						   ->where('establecimiento.id', $id)
						   ->fetch();
		if ($obtener != false) {
			$subcategorias = false;
			//existe subcategorias
			if (isset($data['subcategorias'])) {
				$subcategorias = $data['subcategorias'];
				unset($data['subcategorias']);
				// se eliminan las anteriores 
				$eliminarSubcategorias = $this->db->deleteFrom($this->tableHasSubTypeEstablishment)
												  ->where('idEstablecimiento',$id)
												   ->execute();
				// alta nuevas subcategorias
				$arraySubcategorias = explode("-",$subcategorias);
				foreach ($arraySubcategorias as $value) {
					$dataSub = array(
						"idEstablecimiento"=>$id,
						"idSubCategorias"=>$value
					);
					$insertarSubcategorias = $this->db->insertInto($this->tableHasSubTypeEstablishment,$dataSub)
													  ->execute();
				}
			}
			// id persona??
			if (count($data) > 0) {
						if ($data['idPersona'] == 0) {
							$data['idPersona'] = NULL;
						}
					   $actualizar = $this->db->update($this->tableEstablishment, $data) 
											  ->where('id',$id)          
											  ->execute();

					   $this->response->result = $data;
				return $this->response->SetResponse(true);
			}else {
				if ($subcategorias != false) {
							$this->response->result = $subcategorias;
					return 	$this->response->SetResponse(true);
				}else{
							$this->response->errors = "no se actualizo ningun dato";
					return 	$this->response->SetResponse(false);
				}
			}
			
		}else{
				   $this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(true);
		}
	}
	//si funciona
    public function delete($id){
    	$this->db->update($this->tableEstablishment)
				 ->set('status', 'inactivo') #set actualiza la columna indicada por el valor indicado
				 ->where('id', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
    }
    #listar categorias y sub categorias
	//si funciona
    public function listCategories(){
    	$data = $this->db->from($this->tableTypeEstablishment)
    					 ->select(null)
						 ->select('categoria.id, descripcion, urlImg')
						 ->where('status','activo')
						 ->orderBy('id DESC') #ASC
						 ->fetchAll();
						 
    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
    }
	//SI FUNCIONA
	public function listSubcategories($id){
		$data = $this->db->from($this->tableSubTypeEstablishment)
						 ->where('idCategoria',$id)
						 ->where('status','activo')
    					 ->fetchAll();

    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
	}
	//si funciona
	public function addCategoria($data){
		// alta de establecimiento
		$register = $this->db->insertInto($this->tableTypeEstablishment, $data)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	//SI FUNCIONA
	public function addSubcategoria($data){
		// alta de subcategoria
		$register = $this->db->insertInto($this->tableSubTypeEstablishment, $data)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	//SI FUNCIONA
	public function updateCategoria($data,$id){
		// actualizar categoria
		$register = $this->db->update($this->tableTypeEstablishment, $data)
							 ->where('id',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	//SI FUNCIONA
	public function updateSubcategoria($data,$id){
		// actualizar subcategoria
		$register = $this->db->update($this->tableSubTypeEstablishment, $data)
							 ->where('id',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	//SI FUNCIONA
	public function deleteCategoria($id){
		$register = $this->db->update($this->tableTypeEstablishment)
							 ->set('status','inactivo')
							 ->where('id',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	//SI FUNCIONA
	public function deleteSubcategoria($id){
		$register = $this->db->update($this->tableSubTypeEstablishment)
							 ->set('status','inactivo')
							 ->where('id',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	#Si Funciona
	public function list(){
		$data = $this->db->from($this->tableEstablishment)
						 ->select(null)
						 ->select("*")
						 ->fetchAll();

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}
    //si funciona
	#ADMIN ESTABLECIMIENTO
	public function listAdmin(){
		$data = $this->db->from($this->tableEstablishment)
						//  ->select(null)
						//  ->select('establecimiento.idEstablecimiento, Nombre AS NombreEstablecimiento, Direccion, establecimiento.UrlFoto, IFNULL(establecimiento.Latitud, 0) AS Latitud, IFNULL(establecimiento.Longitud, 0) AS Longitud, categoria.Descripcion AS Categoria')
						//  ->leftJoin('categoria on categoria.idCategoria = establecimiento.idCategoria')
						 ->where('status','activo')
						 ->groupBy('establecimiento.id')
						 ->orderBy('id DESC')
						 ->fetchAll();

		// $data = $this->agregarSubcaterias($data);

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}
	//pendienete
	public function agregarSubcaterias($data){
		foreach ($data as $element) {
			// $element["idEstablecimiento"];
			$subcategorias = $this->db->from($this->tableHasSubTypeEstablishment)
									->select()
									->select('subcategorias.Descripcion')
									->leftJoin('subcategorias on subcategorias.idSubCategorias = categoria_has_subcategorias.idSubCategorias')
									->where('idEstablecimiento',$element->idEstablecimiento)
									->fetchAll();
			$cadena = '';
			foreach ($subcategorias as $value) {
				$cadena.="$value->Descripcion,";
			}
			$cadena = substr($cadena, 0, -1);
			$element->Categoria = $cadena;
		}
		return $data;
	}
	//Servicios de detalle de establecimiento y un arreglo de las promociones que ofrece
	//si funciona
	public function detailEstablishment($id){
		$detalleNegocio = $this ->db->from($this->tableEstablishment)
										->where('id', $id)
										->fetch();           
		if	($detalleNegocio != false)	{
			$obtenerPromos = $this->db->from($this->tablePromos)
		                          ->select(null)
								  ->select("id,titulo,descripcion")
								  ->where("idEstablecimiento",$id)
								  ->fetchAll(); //convertir objeto en arreglo
	
			$detalleNegocio->promos = $obtenerPromos;

			       $this->response->result = $detalleNegocio;		
			return $this->response->SetResponse(true);
		}else{
			       $this->response->errors='No existen negocios';
			return $this->response->SetResponse(false);
		}
	}
	//SI FUNCIONA
	public function listApp(){
		$data = $this->db->from($this->tableEstablishment)
						 ->select(null)
						 ->select('id,nombre,descripcion,urlImgPerfil, Calificacion')
						 ->where('status','activo')
						 ->where('tipo','establecimiento')
						 ->orderBy('RAND()')
						 ->fetchAll();

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}
	public function listCentro(){
		$data = $this->db->from($this->tableEstablishment)
		->select(null)
		->select('id,nombre,urlImg')
		->where('status','activo')
		->where('centroAutorizado', 'y')
		->orderBy('RAND()')
		->fetchAll();

$this->response->result= $data;
return $this->response->SetResponse(true);
	}

	public function listExperiencies(){
		$data = $this->db->from($this->tableEstablishment)
						 ->select(null)
						 ->select('id,nombre,urlImgPerfil')
						 ->where('status','activo')
						 ->where('tipo','experiencia')
						 ->orderBy('RAND()')
						 ->fetchAll();

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}

}