<?php
namespace App\Model;

use App\Lib\Response;

class MembershipModel{

    private $db;
    private $tbMembresia = 'membresia';
    private $tbUser = 'persona';
    private $tbQrFisico = 'codigoQrFisico';
    private $tbQrVirtual = 'codigoQrVirtual';
    private $tbPlanes = 'planes';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }
    //si funciona
    public function add($correo, $plan){
        $busca_usuario = $this->db->from($this->tbUser)
            ->select(null)
            ->select('id')
            ->where("correo = :correo", array(
                ":correo" => $correo
            ))
            ->fetch();

        if ($busca_usuario) {
            $registro = $this->db->from($this->tbMembresia)
                                 ->select(null)
                                 ->select('idMembresia, fechaExpiracion')
                                 ->where("idPersona = '$busca_usuario->id' AND status = 1")
                                 ->orderBy('idMembresia DESC')
                                 ->fetch();
            if ($registro) {
                $dias = $this->db->from($this->tbPlanes)
                                 ->select(null)
                                 ->select('tiempoEnDias')
                                 ->where("id = '$plan'")
                                 ->fetch();
                    
                if($dias){
                    $fecha_envio = date("Y-m-d H:i:s", (strtotime($registro->fechaExpiracion . "+ 1 second")));
                    $fecha_expiracion = date('Y-m-d 23:59:59', (strtotime ($fecha_envio . "+ $dias->tiempoEnDias Days")));
    
                    $data_membresia = [
                        'FechaUltimoPago' => $fecha_envio,
                        'FechaExpiracion' => $fecha_expiracion,
                        'idPersona' => $busca_usuario->id,
                        'plan_id' => $plan
                    ];
    
                    $altaCodigo = $this->db->insertInto($this->tbMembresia, $data_membresia)
                                           ->execute();
                    
                    if ($altaCodigo) {
                        $this->response->result = $altaCodigo;
                        return $this->response->SetResponse(true, 'Se agregó con éxito la membresía al usuario con el correo: ' . $correo);
                    } else {
                        $this->response->result = [];
                        return $this->response->SetResponse(false, 'Ocurrió un problema al agregar la membresía al usuario con correo: ' . $correo . ' por favor inténtelo nuevamente.');
                    }
                }else{
                    $this->response->errors = "No se ha podido agregar la membresia con este plan.";
                    return $this->response->SetResponse(false);
                }
            } else {
                $dias = $this->db->from($this->tbPlanes)
                                 ->select(null)
                                 ->select('tiempoEnDias')
                                 ->where("id = '$plan'")
                                 ->fetch();
                    
                if($dias){
                    $fecha_envio = date('Y-m-d H:i:s');
                    $fecha_expiracion = date('Y-m-d 23:59:59', (strtotime ($fecha_envio."+ $dias->tiempoEnDias Days")));
    
                    $data_membresia = [
                        'FechaUltimoPago' => $fecha_envio,
                        'FechaExpiracion' => $fecha_expiracion,
                        'idPersona' => $busca_usuario->id,
                        'plan_id' => $plan
                    ];
    
                    $altaCodigo = $this->db->insertInto($this->tbMembresia, $data_membresia)
                                            ->execute();
    
                    if ($altaCodigo) {
                        $this->response->result = $altaCodigo;
                        return $this->response->SetResponse(true, 'Se agregó con éxito la membresía al usuario con el correo: ' . $correo);
                    } else {
                        $this->response->result = [];
                        return $this->response->SetResponse(false, 'Ocurrió un problema al agregar la membresía al usuario con correo: ' . $correo . ' por favor inténtelo nuevamente.');
                    }
                }else{
                    $this->response->errors = "No se ha podido agregar la membresia con este plan.";
                    return $this->response->SetResponse(false);
                }
            }
        } else {
                $this->response->errors = "Este usuario no está registrado.";
            return $this->response->SetResponse(false);
        }

    }
    //lista
    //FUNCIONA 
    public function list(){
        $listar = $this->db->from($this->tbMembresia)
                         ->select(null)
                         ->select('idMembresia, membresia.idPersona, FechaUltimoPago, FechaExpiracion, CONCAT(persona.Nombre, " ",persona.Apellidos) as Usuario')
                         ->leftJoin('persona on membresia.idPersona = persona.id')
                         ->orderBy('idMembresia DESC') #ASC
						 ->fetchAll();

		if	($listar !=false)	{
				   $this->response->result=['Data' => $listar];
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe promocion';
			return $this->response->SetResponse(false);
		}
    }

    //Detalle de membresia por id de persona y fecha actual
    //SI FUNciona
    public function detail($id){
        $fecha = date('Y-m-d H:i:s');
        $detail = $this->db->from($this->tbMembresia)
                         ->select(null)
                         ->select('*')
                         ->where("idPersona = '$id' AND fechaUltimoPago <= '$fecha' AND fechaExpiracion >= '$fecha'")
                         ->orderBy('idMembresia DESC')
                         ->fetch();

        if	($detail != false) {
                   $this->response->result=$detail;
            return $this->response->SetResponse(true);
        }else{
            $detail2 = $this->db->from($this->tbMembresia)
                         ->select(null)
                         ->select('*')
                         ->where("idPersona = '$id'")
                         ->orderBy('idMembresia DESC')
                         ->fetch();
            if ($detail2) {
                $this->response->result=$detail2;
                return $this->response->SetResponse(true);
            } else {
                $this->response->errors='Error, esta membresia no existe o ya ha terminado';
                return $this->response->SetResponse(false);
            }
        }
    }

    //Detalle de promociones por id
    //SI FUNciona
    public function detailForId($id){
        $detail = $this->db->from($this->tbMembresia)
                         ->select(null)
                         ->select('*')
                         ->where('idMembresia', $id)
                         ->fetch();

        if	($detail != false) {
                   $this->response->result=$detail;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Error, esta membresia no existe';
            return $this->response->SetResponse(false);
        }
    }

    // update
    //SI FUNCIONA
    public function update($data,$id){
        $addMembership = $this->db->update($this->tbMembresia, $data)
                                        ->where('idMembresia',$id)
                                        ->execute();
		
	    if ($addMembership != false) {
				    $this->response->result = $addMembership;
		    return $this->response->SetResponse(true,'Actualizdo con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }
    //Eliminar
    //SI FUNCIONA
    public function delete($idMembresia){
		$eliminar = $this->db->deleteFrom($this->tbMembresia)
				 ->where('idMembresia',$idMembresia)
				 ->execute();

		if	($eliminar != false) {
				   $this->response->result=$eliminar;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				   $this->response->errors='Error, al eliminar la membresia, verifique nuevamente!!';
			return $this->response->SetResponse(false);
        }
    }
    
    //Crea el registro para poder crear el qr de el huauchipass del usuario
    public function validatePass($idP){
        $validarQrF = $this->db->from($this->tbQrFisico)
                                ->select(null)
                                ->select('idPersona')
                                ->where('idPersona',$idP)
                                ->fetch();
        if($validarQrF != false){
            $cadena = $validarQrF->idPersona;
            $codgo=base64_encode("F-".$cadena);
                $this->response->result = $codgo;
                return $this->response->SetResponse(true,"Id F codificado con exito");
        }else{
            $validarQrV = $this->db->from($this->tbQrVirtual)
                                ->select(null)
                                ->select('idPersona')
                                ->where('idPersona',$idP)
                                ->fetch();
            if($validarQrV==true){
                $cadena = $validarQrV->idPersona;
            $codgo=base64_encode("V-".$cadena);
                $this->response->result = $codgo;
                return $this->response->SetResponse(true,"Id V codificado con exito");
            }else{
                $dataPass = [
                    'idPersona' => $idP
                ];
                $altaCodigo = $this->db->insertInto($this->tbQrVirtual, $dataPass)
                                       ->execute();
                $this->response->result = $altaCodigo;
                $codgo=base64_encode("V-$altaCodigo");
                $this->response->result = $codgo;
                return $this->response->SetResponse(true,"Insercion con exito");        
                
            }
            
        }
    }

    // Genera el qr para el huauchipass del usuario
    public function buscarQr($idP){
        $validarQrF = $this->db->from($this->tbQrFisico)
                                ->select(null)
                                ->select('idPersona')
                                ->where('idPersona',$idP)
                                ->fetch();
        if($validarQrF != false){
            $cadena = $validarQrF->idPersona;
            $codgo=base64_encode("F-".$cadena);
                $this->response->result = $codgo;
                return $this->response->SetResponse(true,"Id F codificado con exito");
        }else{
            $validarQrV = $this->db->from($this->tbQrVirtual)
                                ->select(null)
                                ->select('idPersona')
                                ->where('idPersona',$idP)
                                ->fetch();
            if($validarQrV==true){
                $cadena = $validarQrV->idPersona;
            $codgo=base64_encode("V-".$cadena);
                $this->response->result = $codgo;
                return $this->response->SetResponse(true,"Id V codificado con exito");
            }else{
                return $this->response->SetResponse(false,"Este código no existe");        
                
            }
            
        }
    }

    // Agrega un plan para las membresias del huauchipass
    public function registrarPlan($nombre, $precio, $tiempo){
        $data_plan = [
            'nombre' => $nombre,
            'precio' => $precio,
            'tiempoEnDias' => $tiempo
        ];

        $plan_nombre = $this->db->from($this->tbPlanes)
                                ->select(null)
                                ->select('*')
                                ->where("nombre = '$nombre'")
                                ->fetch();

        if($plan_nombre){
            $this->response->result = [];
            return $this->response->SetResponse(false, "Ya existe un plan con este nombre");
        }else{
            $data = $this->db->insertInto($this->tbPlanes, $data_plan)
                             ->execute();
            $this->response->result = "Éxito";
            return $this->response->SetResponse(true, "Se agrego correctamente el nuevo plan");
        }
    }

    // Obtiene una lista de todos los planes que pueden aplicarse a un huauchipass
    public function obtenerPlanes(){
        $planes = $this->db->from($this->tbPlanes)
                                ->select(null)
                                ->select('*')
                                ->fetchAll();
        if($planes){
            $this->response->result = $planes;
            return $this->response->SetResponse(true, "Exito");
        }else{
            $this->response->result = [];
            return $this->response->SetResponse(false, "Error al obtener los planes");
        }
    }

    //Obtiene todos los detalles de un solo plan
    public function obtenerPlan($id){
        $planes = $this->db->from($this->tbPlanes)
                                ->select(null)
                                ->select('*')
                                ->where("id = '$id'")
                                ->fetch();
        if($planes){
            $this->response->result = $planes;
            return $this->response->SetResponse(true, "Exito");
        }else{
            $this->response->result = [];
            return $this->response->SetResponse(false, "Error al obtener el plan");
        }
    }

    // Actualiza la infromacion de un plan
    public function actualizarPlan($data, $id){
        $update = $this->db->update($this->tbPlanes, $data)
                           ->where('id',$id)
                                        ->execute();
		
	    if ($update) {
				    $this->response->result = $update;
		    return $this->response->SetResponse(true,'Actualizdo con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }

    // Elimina un plan
    public function eliminarPlan($id){
        $eliminar = $this->db->deleteFrom($this->tbPlanes)
				 ->where('id',$id)
				 ->execute();

		if	($eliminar) {
				    $this->response->result=$eliminar;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				    $this->response->errors='Error, al eliminar el plan, intente nuevamente!!';
			return $this->response->SetResponse(false);
        }
    }
}