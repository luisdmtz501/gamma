<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Codigos,
    App\Lib\RestApi,
    App\Lib\RestApiMethod,
    App\Lib\Token,
    App\Lib\Email,
    App\Lib\EmailRecoverPassword;

class AuthModel
{
    private $db;
    private $tbUser = 'persona';
    private $tbEstablecimiento = 'establecimiento';
    private $tbRecuperarPassword = 'recuperarpassword';
    private $tbTokenFirebase = 'tokenpush';
    private $tbsms='mensajes';
    private $tbRemoval = 'solicitudEliminarCuenta';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    
    #LOGIN USUARIO
    public function autenticar($user, $password,$tipopersona,$token, $plataforma) {
        $password = Cifrado::Sha512($password);
        if ($tipopersona == 2) {
            $persona = $this->db->from($this->tbUser)
                            ->select(null)
                            ->select('persona.id, persona.nombre, persona.apellidos, persona.telefono, persona.correo, persona.urlImg, persona.tipoPersona, persona.status, establecimiento.Descripcion, establecimiento.Nombre AS nombreEstab, establecimiento.urlImgPerfil, establecimiento.id AS idEstab')
                            ->leftJoin('establecimiento on establecimiento.idPersona = persona.id')
                             ->where("
                                (persona.status = 1) AND
                                (persona.tipoPersona = :tipopersona AND password = :password) AND
                                (persona.telefono = :telefono or correo = :correo )
                                ", array(
                                    ':tipopersona' => $tipopersona,
                                    ':password' => $password,
                                    ':telefono' => $user,
                                    ':correo' => $user
                                ))                                
                                ->fetch();
            
        }else {
            $persona = $this->db->from($this->tbUser)
                            ->where("
                                (persona.status = 'activo' ) AND
                                (persona.tipoPersona = :tipopersona AND Password = :password) AND
                                (telefono = :telefono or correo = :correo )
                                ", array(
                                    ':tipopersona' => $tipopersona,
                                    ':password' => $password,
                                    ':telefono' => $user,
                                    ':correo' => $user
                                ))
                            ->fetch();
                            //  ->getQuery();
                            //  echo $persona;
                            //  die;
        }
   
        $estabs = null;
        if(is_object($persona)){
            //actualizar activo en mysql
             $actualizarActivo = $this->db->update($this->tbUser, ["status"=>"activo"])
                                          ->where('id', $persona->id)
                                          ->execute();
            //crea el nodo de Firebase
            RestApi::call(
                RestApiMethod::POST,
                "$persona->Descripcion/$persona->id.json",
                []);
            //actualiza el nodo con la informacion del usuario 
            RestApi::call(
                RestApiMethod::PUT,
                "$persona->Descripcion/$persona->id.json",
                [
                    'idpersona' => $persona->id,
                    'Nombre'    => $persona->nombre,
                    'apellidos' => $persona->apellidos,
                    'status'    => $persona->status,
                    'correo'    => $persona->correo,
                    'telefono'  => $persona->telefono
                    //'codigo_pais'  => $usuario->codigo_pais
                    //'latitud'   => 0,
                    //'longitud'  => 0,
                    //'activo'    => 1
                ]
            );
            if ($tipopersona == '2') {
                $estabs = $this->db->from($this->tbEstablecimiento)
                                ->select(null)
                                ->select('nombre, id')
                                ->where("status = 'activo' AND idPersona = '$persona->id'")
                                ->fetchAll();

                $token = Token::addToken([
                    'idPersona'      => $persona->id,
                    'Nombre'         => $persona->nombre,
                    'Apellidos'      => $persona->apellidos,
                    'Telefono'       => $persona->telefono,
                    'idTipoPersona'  => $persona->tipoPersona,
                    'urlImg'         => $persona->urlImg,
                    'NombreEstab'    => $persona->nombreEstab,
                    'idEstab'        => $persona->idEstab,
                    'Correo'         => $persona->correo,
                ]);
            } else {
                $token = Token::addToken([
                    'idPersona'      => $persona->id,
                    'Nombre'         => $persona->nombre,
                    'Apellidos'      => $persona->apellidos,
                    'Telefono'       => $persona->telefono,
                    'idTipoPersona'  => $persona->tipoPersona,
                    'urlImg'         => $persona->urlImg,
                    'Correo'         => $persona->correo
                ]);
            }
            
             // token firebase
            //  if($tokenFB != null){
            //     $data = [
            //         'idPersona' => $persona->id,
            //         'Token' => $tokenFB,
            //         'Plataforma' => $plataforma
            //     ];

            //     $idUsuario = $data['idPersona'];
            //     $idToken = $this->db->from($this->tbTokenFirebase)
            //                         ->select('COUNT(*) Total')
            //                         ->where('idPersona', $idUsuario)
            //                         ->fetch()
            //                         ->Total;

            //     if ($idToken > 0) {
            //         $actualizatoken = $this->db->update($this->tbTokenFirebase, $data)
            //                                    ->where('idPersona', $idUsuario)
            //                                    ->execute();
            //     }else{
            //         $altatoken=$this->db->insertInto($this->tbTokenFirebase, $data)
            //                             ->execute();
            //     }
            // }

            $deletion_verification = $this->db->from($this->tbRemoval)
                                              ->select(null)
                                              ->select("id_persona, fechaSolicitud, status")
                                              ->where("id_persona = '$persona->id'")
                                              ->fetch();
            if ($deletion_verification) {
                if ($deletion_verification->status == "Pendiente") {
                    $this->response->result = "Request";
                    return $this->response->SetResponse(false, "Esta cuenta tiene una solicitud de eliminación realizada el dia " . $deletion_verification->fechaSolicitud . " (la solicitud se atendera en un periodo maximo de 7 dias), por lo que no se puede iniciar sesion, si tienes alguna duda o problema porfavor comunicate con soporte");
                } 
            } else {
                $this->response->result = [ 'token' => $token,
                                            'id'    => $persona->id,
                                            'estabs' => $estabs
                                           ];
    
                return $this->response->SetResponse(true);
            }
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");                 
        }
    }
    #CERRAR SESION
    //pendiente
    public function logout($id){
        $actualizarActivo = $this->db->update($this->tbUser, ["status"=>"inactivo"])
                                     ->where('id', $id)
                                     ->execute();

                $this->response->result = "se cerro la sesion";
        return $this->response->SetResponse(true);
    }
    #GET DATA
    //SI FUNCIONA
    public function getData($data){
        $token = new Token();
        $res = $token->getData($data);
        if ($res === null) {
                   $this->response->errors = "Token incorrecto"; 
            return $this->response->SetResponse(false);
        }
               $this->response->result = $res;
        return $this->response->SetResponse(true);
    }

    #Enviar el código al correo(recovery pass)
    public function sendCodeEmail($email, $tipeUser){
        $busca_correo_registrado = $this->db->from($this->tbUser)
                                            ->select(null)
                                            ->select('CONCAT(nombre, " ", apellidos) AS NombreCompleto, id')
                                            ->where("
                                                tipoPersona = :tipo AND 
                                                correo = :email AND 
                                            (status = 'activo')",
                                                array(
                                                    ':tipo' => $tipeUser,
                                                    ':email' => $email
                                                ))
                                            ->fetch();

        if ($busca_correo_registrado != false) {
            $codigo_verificacion = Codigos::Codigo(6);
            
            #Mandar al correo electrónico

            $send_email = EmailRecoverPassword::sendEmail($email, $codigo_verificacion, $busca_correo_registrado->NombreCompleto);

            // $send_email = 1;

            if ($send_email == 1) {

                $inval =$this->db->from($this->tbRecuperarPassword)
                                 ->select(null)
                                 ->select("id")
                                 ->where("correo = :mail AND tipoPersona = :tipo",
                                 array(
                                    ':mail'=> $email,
                                    ':tipo'=> $tipeUser
                                 ))
                                 ->orderBy("id DESC")
                                 ->fetch();
                
                if($inval){
                    $updateStaus = $this->db->update($this->tbRecuperarPassword)
                                        ->set('status','inactivo')
                                        ->where('id', $inval->id)
                                        ->execute();
                }

                $fecha_envio = date('Y-m-d H:i:s');
                $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));

                $data_mensaje = [
                    'codigo' => $codigo_verificacion,
                    'fechaAlta' => $fecha_envio,
                    'fechaExpiracion' => $fecha_expiracion,
                    'correo' => $email,
                    'status' => 1,
                    'idPersona' => $busca_correo_registrado->id,
                    'tipoPersona'=>$tipeUser
                ];

                $altaCodigo = $this->db->insertInto($this->tbRecuperarPassword, $data_mensaje)
                                        ->execute();

                       $this->response->result = $busca_correo_registrado->NombreCompleto;
                return $this->response->SetResponse(true, 'Se ha enviado un código de verifiación al correo: '.$email);
            } else {
                       $this->response->errors = "No se ha podido enviar el correo electrónico.";
                return $this->response->SetResponse(false);
            }
        } else {
                   $this->response->errors = "El correo proporcionado no está asociado a una cuenta.";
            return $this->response->SetResponse(false);
        }
    }
    #Validar el código
    //si funciona
    public function validateCodeEmail($correo,$code,$tipopersona){
        $fecha_val = date('Y-m-d H:i:s');
        $data_mensaje = $this->db->from($this->tbRecuperarPassword)
                                ->select(null)
                                ->select('id AS idRecover, codigo')
                                ->where("id = (SELECT MAX(id) FROM recuperarpassword 
                                    WHERE correo = :correo AND
                                    status = 'activo' AND tipoPersona = :tipopersona 
                                    AND fechaAlta < :hoy AND fechaExpiracion > :hoy)",
                                    array(
                                        ':correo' => $correo,
                                        ':tipopersona'=>$tipopersona,
                                        ':hoy' => $fecha_val
                                    ))
                                ->fetch();


        if ($data_mensaje != false) {
            if ($data_mensaje->codigo === $code) {
                $updateStaus = $this->db->update($this->tbRecuperarPassword)
                                        ->set('status','inactivo')
                                        ->where('id', $data_mensaje->idRecover)
                                        ->execute();

                $token_recover_password = Token::logRecoverPassword(["tokenRegistro" => $code]);

                       $this->response->result = $token_recover_password;
                return $this->response->SetResponse(true, 'Código de verificación correcto.');
            } else {
                       $this->response->errors='El código que ingresó es incorrecto.';
                return $this->response->SetResponse(false); 
            }
        } else {
                   $this->response->errors='No hay código para este email.';
            return $this->response->SetResponse(false);
        }
    }

    public function validateEmail($email,$tipopersona){
         $getemail = $this->db->from($this->tbUser)
                                // ->where('correo',$email)
                                ->where("id = (SELECT MAX(id) FROM persona 
                                    WHERE correo = :correo AND
                                    status = 'activo' AND tipoPersona = :tipopersona)",
                                    array(
                                        ':correo' => $email,
                                        ':tipopersona'=>$tipopersona
                                    ))
                                ->fetch();
                                
        if( $getemail != false){
            $this->response->result = $email;
            return $this->response->SetResponse(false, 'Este correo electronico ya ha sido registrado');
        }else{
            $codigo_verificacion = Codigos::Codigo(6);
            
            #Mandar al correo electrónico
            $send_email = Email::Send($email, $codigo_verificacion);

            // $send_email = 1;

            if ($send_email == 1) {

                $inval =$this->db->from($this->tbsms)
                                 ->select(null)
                                 ->select("idmensajes")
                                 ->where("temcorreo = :mail AND tipoPersona = :tipo",
                                 array(
                                    ':mail'=> $email,
                                    ':tipo'=> $tipopersona
                                 ))
                                 ->orderBy("idmensajes DESC")
                                 ->fetch();
                
                if($inval){
                    $updateStaus = $this->db->update($this->tbsms)
                                        ->set('status','inactivo')
                                        ->where('idmensajes', $inval->idmensajes)
                                        ->execute();
                }

                $fecha_alta = date('Y-m-d H:i:s');
                $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));

                $data = [
                    'codigo' => $codigo_verificacion,
                    'fechaAlta' => $fecha_alta,
                    'fechaExpiracion' => $fecha_expiracion,
                    'temcorreo' => $email,
                    'status' => 1,
                    'tipoPersona' => $tipopersona
                ];

                $altaCodigo = $this->db->insertInto($this->tbsms, $data)
                                        ->execute();
                
                
                  $this->response->result = $email;
                  return $this->response->SetResponse(true, 'Se ha enviado un código de verifiación al correo: '. $email);   
            }else {
                $this->response->result = $email;
                return $this->response->SetResponse(false, 'No se ha podido enviar el correo electrónico.');   
            }
                  
        }
    }

    public function validateCode($correo, $code, $tipopersona){
        $fecha_val = date('Y-m-d H:i:s');
        $data_mensaje = $this->db->from($this->tbsms)
                                ->select(null)
                                ->select('idmensajes AS idRecover, codigo')
                                ->where("idmensajes = (SELECT MAX(idmensajes) FROM mensajes 
                                    WHERE temcorreo = :email AND tipopersona = :tipoper AND
                                    status = 'activo' AND fechaAlta < :hoy AND fechaExpiracion > :hoy)",
                                    array(
                                        ':email' => $correo,
                                        'tipoper' => $tipopersona,
                                        ':hoy' => $fecha_val
                                    ))
                                ->fetch();


        if ($data_mensaje != false) {
            
            if ($data_mensaje->codigo === $code) {
                $updateStaus = $this->db->update($this->tbsms)
                                        ->set('status', 'inactivo')
                                        ->where('idmensajes', $data_mensaje->idRecover)
                                        ->execute();

                $token_recover_password = Token::logRecoverPassword(["tokenRegistro" => $code]);

                       $this->response->result = $token_recover_password;
                return $this->response->SetResponse(true, 'Código de verificación correcto.');
            } else {
                       $this->response->errors='El código que ingresó es incorrecto.';
                return $this->response->SetResponse(false); 
            }
        } else {
                   $this->response->errors='No hay código para este email.';
            return $this->response->SetResponse(false);
        }
    }

    public function checkPassword($id, $pass){
        $data_request = $this->db->from($this->tbUser)
                             ->select(null)
                             ->select("id, password")
                             ->where("id = '$id'")
                             ->fetchAll();
        if ($data_request) {
            $pass = Cifrado::Sha512($pass);
            if($data_request[0]->password == $pass){
                $this->response->result = $data_request;
                return $this->response->SetResponse(true, 'Contraseña correcta');
            } else {
                $this->response->result = [];
                return $this->response->SetResponse(false, 'Contraseña incorrecta');
            }
        } else {
            $this->response->result = $data_request;
            return $this->response->SetResponse(false, 'Esta cuenta no existe');
        }
    }

    public function removalRequest($id){
        $data_request = $this->db->from($this->tbUser)
                             ->select(null)
                             ->select("id, password")
                             ->where("id = '$id'")
                             ->fetchAll();
        if ($data_request) {
            $data_request2 = $this->db->from($this->tbRemoval)
                                 ->select(null)
                                 ->select("id, status")
                                 ->where("id_persona = '$id'")
                                 ->fetchAll();
            
            if(!$data_request2){
                $application_date = date('Y-m-d H:i:s');
                $data = [
                    'fechaSolicitud' => $application_date,
                    'id_persona' => $id,
                    'status' => 1
                ];
                $data_request3 = $this->db->insertInto($this->tbRemoval, $data)
                                 ->execute();
    
                $this->response->result = $data_request3;
                return $this->response->SetResponse(true, 'La solicitud para eliminar esta cuenta se ha realizado correctamente y será atendida en un máximo de 7 días.');
            } else {
                if($data_request2[0]->status == 'Pendiente'){
                    $this->response->result = $data_request2[0]->status;
                    return $this->response->SetResponse(false, 'Ya se ha hecho una petición de eliminación de esta cuenta');
                } else {
                    $this->response->result = $data_request2[0]->status;
                    return $this->response->SetResponse(false, 'Esta cuenta ya ha sido eliminada');
                }
            }
        } else {
            $this->response->result = $data_request;
            return $this->response->SetResponse(false, 'Esta cuenta no existe');
        }
        
    }
}