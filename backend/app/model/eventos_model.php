<?php
    namespace App\Model;
    
    use App\Lib\Response;
    use App\Lib\PushExpo;

    class EventosModel
    {
        private $db;
        private $response;
        // private $urlActividades = 'http://192.168.1.252/gamma/backend/img/actividades/';
        // private $urlPremios = 'http://192.168.1.252/gamma/backend/img/premios/';
        private $urlActividades = 'https://huauchitour.com/dev/backend/img/actividades/';
        private $urlPremios = 'https://huauchitour.com/dev/backend/img/premios/';

        public function __CONSTRUCT($db)
        {
            $this->db = $db;
            $this->response = new Response();
        }
        
        //Actualiza el progreso del usuario.
        public function registroProgreso($idPersona, $idEvento){
            $data = $this->db->from("actividadesCompletadas")
                              ->select(null)
                              ->select("idActividadesCompletadas")
                              ->leftJoin("actividad ON actividad.idActividad = actividadesCompletadas.actividad_idActividad")
                              ->Where("persona_id = '$idPersona' AND actividad.evento_idEvento = '$idEvento'")
                              ->fetchAll();
                    
                        if ($data) {
                                $progress = count($data);
                                $progress = ["progreso" => $progress];
                                $datos = $this->db->update('persona', $progress, $idPersona)
                                                    ->execute();
                                $this->response->result = $progress;
                                return $this->response->SetResponse(true, "Exito.");
                            }else {
                                return $this->response->SetResponse(false,"No hay eventos completados"); 
                            }
        }
        
        //Generar actividades 
        public function  generarActividad($datosActividad){
            $generar = $this->db->insertInto('actividad', $datosActividad)
                                 ->execute();

            if($generar){
                $this->response->result = $generar;
                return $this->response->setResponse(true, "Exito");
            }else{
                return $this->response->SetResponse(false,"No se pudo registrar la actividad"); 
            }
        }
        
        //Generar evento.
        public function  generarEventos($datosEventos){
            $generar = $this->db->insertInto('evento', $datosEventos)
                                 ->execute();

            if($generar){
                $this->response->result = $generar;
                return $this->response->setResponse(true, "Exito");
            }else{
                return $this->response->SetResponse(false,"No se pudo registrar la actividad"); 
            }
        }
        
        //Generar premio.
        public function  generarPremio($datosPremio){
            $generar = $this->db->insertInto('premio', $datosPremio)
                                 ->execute();

            if($generar){
                $this->response->result = $generar;
                return $this->response->setResponse(true, "Exito");
            }else{
                return $this->response->SetResponse(false,"No se pudo registrar el premio"); 
            }
        }
        
        //Reclamar premio.
        public function reclamarPremio($data, $id){
            if($data == 1){
                $data2 = $this->db->from("premio")
                                 ->select(null)
                                 ->select("reclamado")
                                 ->where("idPremio = '$id' AND reclamado = 1")
                                 ->fetch();
                                 
                if(!$data2){
                    $actualizacion = $this->db->update("premio")
                                        ->set("reclamado", $data)
                                        ->where("idPremio", $id)
                                        ->execute();
        
                    if($actualizacion){
                        $this->response->result = "Status actualizado";
                        return $this->response->setResponse(true, "Exito");
                    }else{
                        return $this->response->setResponse(false,"No pudimos reclamar este premio, porfavor intentalo nuevamente."); 
                    }
                }else{
                    return $this->response->setResponse(false,"Este premio ya ha sido reclamado.");
                }
            } else {
                $data2 = $this->db->from("premio")
                                 ->select(null)
                                 ->select("reclamado")
                                 ->where("idPremio = '$id' AND reclamado = 2")
                                 ->fetch();
                                 
                if(!$data2){
                    $actualizacion = $this->db->update("premio")
                                        ->set("reclamado", $data)
                                        ->where("idPremio", $id)
                                        ->execute();
        
                    if($actualizacion){
                        $this->response->result = "Status actualizado";
                        return $this->response->setResponse(true, "Exito");
                    }else{
                        return $this->response->setResponse(false,"No pudimos actualizar el estado de este premio, porfavor intentelo nuevamente."); 
                    }
                }else{
                    return $this->response->setResponse(false,"Este premio ya ha sido marcado como no reclamado.");
                }
            }
        }

        //Actualizar status de eveto.
        public function actualizarStatusEvento($data, $id){
            $actualizacion = $this->db->update("evento")
                                      ->set("status", $data)
                                      ->where("idEvento", $id)
                                      ->execute();

            if($actualizacion){
                $this->response->result = "Status actualizado";
                return $this->response->setResponse(true, "Exito");
            }else{
                return $this->response->setResponse(true, "No se pudo cambiar el status");
            }
        }

        //Actualizar status de actividad.
        public function actualizarStatusActividad($data, $id){
            $actualizacion = $this->db->update("actividad")
                                      ->set("status", $data)
                                      ->where("idActividad", $id)
                                      ->execute();

            if($actualizacion){
                $this->response->result = "Status actualizado";
                return $this->response->setResponse(true, "Exito");
            }else{
                return $this->response->setResponse(true, "No se pudo cambiar el status");
            }
        }


        //obtiene el evento que se encuentra disponible actualmente en base a la fecha actual
        public function traerEvento()
        {
            $fechaActual = date("Y")."-".date("m")."-".date("d");
            $data = $this->db->from("evento")
                                ->select(null)
                                ->select("*")
                                ->where("fechaInicial <=  '$fechaActual' AND fechaFinal >= '$fechaActual' AND status = 'activo'")
                             ->fetch();//para mas de un registro
        
            if ($data) {
                $this->response->result = $data;
                return $this->response->SetResponse(true, "Exito.");
            }else {
                return $this->response->SetResponse(false,"No hay eventos registrados para esta fecha"); 
            }
        }

        //obtiene el progreso en las actvidades del usuario
        public function traerProgreso($idPersona)
        {
            $data = $this->db->from("persona")
                                ->select(null)
                                ->select("progreso")
                                ->where("id = '$idPersona'")
                                ->fetch();
            if ($data) {
                $this->response->result = $data;
                return $this->response->SetResponse(true, "Exito.");
            }else {
                return $this->response->SetResponse(false,"Aun no se ha realizado ningun progreso"); 
            }
        }

        //registra la poscision en la que quedo el usuario al completar sus actividades
        public function colocarPocision($idEvento, $idPersona)
        {
            //verifica cuantos usuarios ya han completado sus 7 actividades
            $datos = $this->db->from("premiacion")
                                ->select(null)
                                ->select("idPremiacion")
                                ->where("evento_idEvento = $idEvento")
                                ->fetchAll();
            
            $persona = $this->db->from("premiacion")
                                ->select(null)
                                ->select("idPremiacion")
                                ->where("persona_id = $idPersona AND evento_idEvento = $idEvento")
                                ->fetchAll();
            
            //verificar si el usuario ya ha sido registrado con sus actividades completadas 
            if(count($persona) < 1){
                //guarda los datos que se van a insertar
                $registro = ["idPremiacion"=>null, "fecha"=>date("Y")."-".date("m")."-".date("d")." ".date("H").":".date("i").":".date("s"), "puesto"=>count($datos) + 1, "evento_idEvento"=>$idEvento, "persona_id"=>$idPersona];
                $data = $this->db->insertInto('premiacion', $registro)->execute();
                if ($data) {
                    $this->response->result = $data;
                    return $this->response->SetResponse(true, "Exito.");
                }else {
                    return $this->response->SetResponse(false,"No se pudo registrar la entrada"); 
                }
            }else{
                return $this->response->SetResponse(false,"No se pudo registrar la entrada este usuario ya ha completado sus actividades 1 vez"); 
            }
        }

        //obtener la pocision en la que quedo el usuario en un evento
        public function obtenerPocision($idEvento, $idPersona)
        {
            $data = $this->db->from("premiacion")
                                ->select(null)
                                ->select("puesto")
                                ->where("persona_id = $idPersona AND evento_idEvento = $idEvento")
                                ->fetch();
            if ($data) {
                $this->response->result = $data;
                return $this->response->SetResponse(true, "Exito.");
            }else {
                return $this->response->SetResponse(false,"Este usuario no completo todas las actividades en este evento"); 
            }
        }

        //obtener los datos del premio en base al puesto y el evento
        public function obtenerPremio($puesto, $idEvento)
        {
            $data = $this->db->from("premio")
                                ->select(null)
                                ->select("*")
                                ->where("puesto = $puesto AND evento_idEvento = $idEvento")
                                ->fetch();
                    
            if ($data) {
                $data->imagen = $this->urlPremios . $data->imagen;
                if($data->idEstab){
                    $data2 = $this->db->from("establecimiento")
                                ->select(null)
                                ->select("nombre, direccion, longitud, latitud")
                                ->where("id = $data->idEstab AND status = 1")
                                ->fetch();
                    if($data2){
                        $data = (array) $data;
                        $data2 = (array) $data2;
                        $data3 = (object) array_merge($data, $data2);
                        $this->response->result = $data3;
                        return $this->response->SetResponse(true, "Exito.");
                    }else{
                        $this->response->result = $data;
                        return $this->response->SetResponse(true, "El establecimiento buscado no existe o se encuentra inactivo.");
                    }
                }else{
                    $data2 = $this->db->from("centrosAutorizados")
                                ->select(null)
                                ->select("nombreCentro, direccion, longitud, latitud")
                                ->where("id = 1")
                                ->fetch();
                    if($data2){
                        $data = (array) $data;
                        $data2 = (array) $data2;
                        $data3 = (object) array_merge($data, $data2);
                        $this->response->result = $data3;
                        return $this->response->SetResponse(true, "Exito.");
                    }else{
                        $this->response->result = $data;
                        return $this->response->SetResponse(true, "El establecimiento buscado no existe.");
                    }
                }
            }else {
                return $this->response->SetResponse(false,"No hay un premio registrado para este puesto o evento");
            }
        }

        //marcar una actividad como comletada indicando que usuario la completo
        public function registrarActividadesCompletadas($idPersona, $idActividad, $idEstab){
            $registro = ["idActividadesCompletadas"=>null, "fecha"=>date("Y")."-".date("m")."-".date("d")." ".date("H").":".date("i").":".date("s"),"actividad_idActividad"=> $idActividad, "persona_id"=>$idPersona];
            $ac = $this->db->from("actividadesCompletadas")
                              ->select(null)
                              ->select("idActividadesCompletadas")
                              ->where("actividad_idActividad = $idActividad && persona_id = $idPersona")
                              ->fetchAll();
            if ($ac) {
                return $this->response->SetResponse(false,"Esta actividad ya se registro como completada por este usuario");
            }else{
                $estab = $this->db->from("actividad")
                              ->select(null)
                              ->select("idActividad")
                              ->where("establecimiento_id = '$idEstab' && idActividad = '$idActividad'")
                              ->fetchAll();
                if($estab){
                    $data = $this->db->insertInto('actividadesCompletadas', $registro)->execute();
                    if ($data) {
                        $this->response->result = $data;
                        return $this->response->SetResponse(true, "Exito.");
                    }else{
                        return $this->response->SetResponse(false,"No se pudo registrar la actividad como completada"); 
                    }
                }else{
                    return $this->response->SetResponse(false,"Este establecimiento no ha sido asignado a esta actividad"); 
                }
            }
        }

        //traer actividades 
        public function obtenerActividades($idEvento, $idPersona){

            $fechaActual = date("Y")."-".date("m")."-".date("d");

            $data = $this->db->from("actividad")
                                ->select(null)
                                ->select("*")
                                ->where("evento_idEvento = $idEvento AND fechaInicial <=  '$fechaActual' AND fechaFinal >= '$fechaActual' AND status = 'activo'")
                                ->fetchAll();
            if ($data) {
                shuffle($data);
                if(count($data) >= 7){
                    $data7 = [];
                    $dataId = ["idListaActividades"=>null, "actividad_idActividad"=>0, "persona_id"=>$idPersona,"evento_idEvento"=>$idEvento];
                    for ($i=0; $i < 7; $i++) { 
                        array_push($data7, $data[$i]);
                        $data7[$i]->imagen = $this->urlActividades . $data7[$i]->imagen;
                        $dataId['actividad_idActividad'] = $data[$i]->idActividad;
                        $data2 = $this->db->insertInto("listaActividades", $dataId)->execute();
                    }
                    $this->response->result = $data7;
                    return $this->response->SetResponse(true, "Exito.");
                }else{
                    return $this->response->SetResponse(false,"No hay actividades disponibles");
                }
            }else {
                return $this->response->SetResponse(false,"No hay actividades disponibles");
            }
        }

        //verifica si ya se han asignado actividades al usuario
        public function comprobarActividades($idEvento, $idPersona){
            $data = $this->db->from("listaActividades")
                                ->select(null)
                                ->select("*")
                                ->where("evento_idEvento = $idEvento AND persona_id = $idPersona")
                                ->fetchAll();

            if ($data) {
                $this->response->result = true;
                return $this->response->SetResponse(true, "Exito.");
            }else {
                $this->response->result = false;
                return $this->response->SetResponse(false,"Este usuario no ha recibido actividades aun"); 
            }
        }
        // trae las actividades ya asignadas al usuario y verifica si no han sido completadas 
        public function traerListaActividades($idEvento, $idPersona){
            $data = $this->db->from("listaActividades")
                                ->select(null)
                                ->select("actividad_idActividad")
                                ->where("evento_idEvento = $idEvento AND persona_id = $idPersona")
                                ->fetchAll(); 
            if ($data){
                $data2 = $this->db->from("actividadesCompletadas")
                                ->select(null)
                                ->select("actividad_idActividad")
                                ->leftJoin("actividad ON actividadesCompletadas.actividad_idActividad = actividad.idActividad")
                                ->where("persona_id = $idPersona AND actividad.evento_idEvento = $idEvento")
                                ->fetchAll();
                if ($data2) {
                    $listaFinal = [];
                    foreach ($data as $actividad) {
                        array_push($listaFinal, $actividad->actividad_idActividad);
                    } 
                    foreach ($data2 as $actividadC) {
                        $indice = array_search($actividadC->actividad_idActividad ,$listaFinal);
                        unset($listaFinal[$indice]);
                    }
                    $data3 = [];
                    foreach ($listaFinal as $actividad) {
                        $dataA = $this->db->from("actividad")
                                            ->select(null)
                                            ->select("actividad.*, establecimiento.latitud, establecimiento.longitud")
                                            ->leftJoin("establecimiento ON actividad.establecimiento_id = establecimiento.id")
                                            ->where("idActividad = $actividad")
                                            ->fetchAll();
                        $dataA[0]->completado = false;
                        array_push($data3, $dataA[0]);
                    }
                    foreach ($data2 as $actividadComp) {
                        $dataA = $this->db->from("actividad")
                                            ->select(null)
                                            ->select("actividad.*, establecimiento.latitud, establecimiento.longitud")
                                            ->leftJoin("establecimiento ON actividad.establecimiento_id = establecimiento.id")
                                            ->where("idActividad = $actividadComp->actividad_idActividad")
                                            ->fetchAll();
                        $dataA[0]->completado = true;
                        array_push($data3, $dataA[0]);
                    }
                    for ($i=0; $i < count($data3); $i++) { 
                        $data3[$i]->imagen = $this->urlActividades . $data3[$i]->imagen;
                    }
                    $this->response->result = $data3;
                    return $this->response->SetResponse(true, "Exito.");
                }else {
                    $listaFinal = [];
                    foreach ($data as $actividad) {
                        array_push($listaFinal, $actividad->actividad_idActividad);
                    } 
                    $data3 = [];
                    foreach ($listaFinal as $actividad) {
                        $dataA = $this->db->from("actividad")
                                            ->select(null)
                                            ->select("actividad.*, establecimiento.latitud, establecimiento.longitud")
                                            ->leftJoin("establecimiento ON actividad.establecimiento_id = establecimiento.id")
                                            ->where("idActividad = $actividad")
                                            ->fetchAll();

                        $dataA[0]->completado = false;
                        array_push($data3, $dataA[0]);
                    }
                    for ($i=0; $i < count($data3); $i++) { 
                        $data3[$i]->imagen = $this->urlActividades . $data3[$i]->imagen;
                    }
                    $this->response->result = $data3;
                    return $this->response->SetResponse(true, "Exito.");
                }
            } else {
                $this->response->result = false;
                return $this->response->SetResponse(false,"Este usuario no ha recibido actividades aun"); 
            }
        }

        //registro de token en la base de datos para notificaciones
        public function tokenPush($tokenData){
            if($tokenData){
                $data = [
                    'idPersona' => $tokenData['id'],
                    'token' => $tokenData['token'],
                    'plataforma' => $tokenData['plataforma']
                ];

                $idUsuario = $data['idPersona'];
                $idToken = $this->db->from("tokenpush")
                                    ->select(null)
                                    ->select('COUNT(*) Total')
                                    ->where("idPersona = '$idUsuario'")
                                    ->fetch()
                                    ->Total;
                if ($idToken > 0) {
                    $actualizatoken = $this->db->update("tokenpush", $data)
                                               ->where('idPersona', $idUsuario)
                                               ->execute();
                }else{
                    $altatoken=$this->db->insertInto("tokenpush", $data)
                                        ->execute();
                }
                $this->response->result = $data;
                return $this->response->SetResponse(true,"Exito."); 
            }else{
                $this->response->result = false;
                return $this->response->SetResponse(false,"No se recibio token"); 
            }
        }

        //Obtiene token de un usuario y envia la notificacion
        public function getToken($idPersona){
            if($idPersona){
                $data = $this->db->from("tokenpush")
                                    ->Select(null)
                                    ->Select('token')
                                    ->where("idPersona = '$idPersona' AND status = 'activo'")
                                    ->fetch();
                
                if ($data) {
                    $ids = [
                        'idPersona'=>$idPersona,
                        'iden'=>"ADECHT"
                    ];
                    if($data->token != 'closedSession'){
                        $sendNotif=PushExpo::EMC("QREscaneado", $ids, "Listo", $data->token);
                        $this->response->result = true;
                        return $this->response->SetResponse(true,"Exito."); 
                    }else{
                        $this->response->result = false;
                        return $this->response->SetResponse(false,"Token invalido"); 
                    }
                }else{
                    $this->response->result = false;
                    return $this->response->SetResponse(false,"Error no se encontro un token para este usuario"); 
                }
            }else{
                $this->response->result = false;
                return $this->response->SetResponse(false,"Ingresa un id de usuario para continuar"); 
            }
        }
        
        //Borra token para push notifications
        public function deleteTokenByLogoff($idPersona, $token){
            if($idPersona){
                $data = $this->db->from("tokenpush")
                                    ->Select(null)
                                    ->Select('token')
                                    ->where("idPersona = '$idPersona'")
                                    ->fetch();
                
                if ($data) {
                    if($data->token == $token){
                        $data2 = [
                            "token" => 'closedSession'
                        ];
                        $actualizatoken = $this->db->update("tokenpush", $data2)
                                               ->where('idPersona', $idPersona)
                                               ->execute();
                        $this->response->result = true;
                        return $this->response->SetResponse(true,"Exito"); 
                    }else{
                        $this->response->result = false;
                        return $this->response->SetResponse(true,"Este usuario ya ha iniciado sesion en otro dispositivo o ha cerrado su sesion");
                    }
                }else{
                    $this->response->result = false;
                    return $this->response->SetResponse(false,"Error no se encontro un token para este usuario"); 
                }
            }else{
                $this->response->result = false;
                return $this->response->SetResponse(false,"Ingresa un id de usuario para continuar"); 
            }
        }

        // Envia notificaciones a uno o varios usuarios
        public function sendIndiAndGlobalNotifications($id, $type, $title, $body, $iden){
            // $iden se refiere a el tipo de notificacion que se esta enviando que puede ser:
            // PRDHT que se refiere a una notificacion usada para notificar que un premio se ha reclamado y la app de user al recibirlo pueda interpretarlo como una notificacion de premio reclamado
            // ADECHT para completar actividades de evento pero estas ya cuentan con el servicio getToken el cual cumple con este objetivo
            // si $iden viene vacio significa que es una notificacion normal que no realizara ninguna accion exclusiva
            if($type){
                //notificacion para un usuario
                if ($type == 1) {
                    if($title && $body){
                        $data = $this->db->from("tokenpush")
                                            ->Select(null)
                                            ->Select('token')
                                            ->where("idPersona = '$id' AND status = 'activo'")
                                            ->fetch();
                        
                        if ($data) {
                            $info = [
                                'iden'=> $iden
                            ];
                            if($data->token != 'closedSession'){
                                $sendNotif=PushExpo::EMC($title, $info, $body, $data->token);
                                $this->response->result = true;
                                return $this->response->SetResponse(true,"Notificación enviada con éxito."); 
                            }else{
                                $this->response->result = false;
                                return $this->response->SetResponse(false,"Este usuario ha cerrado sesión y no recibirá notificaciones hasta que realice un inicio de sesión."); 
                            }
                        }else{
                            $this->response->result = false;
                            return $this->response->SetResponse(false,"Error no se encontró un token para este usuario."); 
                        }
                    }else{
                        $this->response->result = false;
                        return $this->response->SetResponse(false,"Agrega un título y un mensaje para continuar."); 
                    }
                // notificacion para todos los usuarios
                } else if($type == 2) {
                    if($title && $body){
                        $data = $this->db->from("tokenpush")
                                            ->Select(null)
                                            ->Select('token')
                                            ->where("token != 'closedSession' AND status = 'activo'")
                                            ->fetchAll();
                        
                        if ($data) {
                            $info = [
                                'info'=>"Sin info"
                            ];
                            $this->response->result = $data;
                            foreach ($this->response->result as $tok) {
                                $sendNotif=PushExpo::EMC($title, $info, $body, $tok->token);
                                $this->response->result = true;
                            }
                            return $this->response->SetResponse(true,"Notificaciones enviadas con exito."); 
                        }else{
                            $this->response->result = false;
                            return $this->response->SetResponse(false,"Error no se encontro un token para este usuario."); 
                        }
                    }else{
                        $this->response->result = false;
                        return $this->response->SetResponse(false,"Agrega un título y un mensaje para continuar."); 
                    }
                }
            }else{
                $this->response->result = false;
                return $this->response->SetResponse(false,"Ingresa un tipo de notificación."); 
            }
        }

        // Obtiene el texto de informacion para el usuario para los diferentes modal segun el que se solicite
        public function getInformative($params){
            $modal = $params['modal'];
            $data = $this->db->from("informative")
                             ->select(null)
                             ->select("textInformative")
                             ->where("informativeModal = $modal")
                             ->fetch();
            if ($data) {
                $this->response->result = $data;
                    return $this->response->SetResponse(true,"Información obtenida."); 
            } else {
                $this->response->result = false;
                    return $this->response->SetResponse(false,"Error no se encontro información relacionada a este modal."); 
            }
        }

        // Agrega los textos para los modal informativos
        public function createInformative($params){
            $data = $this->db->insertInto("informative", $params)
                             ->execute();
            if ($data) {
                $this->response->result = [];
                    return $this->response->SetResponse(true,"Información agregada."); 
            } else {
                $this->response->result = false;
                    return $this->response->SetResponse(false,"Error al agregar información."); 
            }
        }
        
        // Actualiza los textos para los modal informativos
        public function updateInformative($params){
            $data = $this->db->update("informative", $params)
                             ->where("id", $params['id'])
                             ->execute();
            if ($data) {
                $this->response->result = [];
                    return $this->response->SetResponse(true,"Información actualizada."); 
            } else {
                $this->response->result = false;
                    return $this->response->SetResponse(false,"Error al actualizar información."); 
            }
        }

        // Elimina los textos para los modal informativos
        public function deleteInformative($params){
            $id = $params['id'];
            $data = $this->db->deleteFrom("informative")
                             ->where('id',$id)
                             ->execute();
            if ($data) {
                $this->response->result = [];
                    return $this->response->SetResponse(true,"Se ha borrado con exito"); 
            } else {
                $this->response->result = false;
                    return $this->response->SetResponse(false,"Error no se encontro el elemento para borrar."); 
            }
        }

    }

?>