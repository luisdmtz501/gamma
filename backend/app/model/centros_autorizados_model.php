<?php
namespace App\Model;

use App\Lib\Response;

class CentrosAutorizados{

    private $db;
    private $response;
    private $tbCentros = "centrosAutorizados";

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function obtenerCentros(){
        $data = $this->db->from($this->tbCentros)
                         ->select(null)
                         ->select("*")
                         ->fetchAll();
        if($data){
            $this->response->result = $data;
            return $this->response->SetResponse(true, "Éxito.");
        }else{
            return $this->response->SetResponse(false, "No hay centros autorizados registrados");
        }
    }
}

?>