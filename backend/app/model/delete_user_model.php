<?php
namespace App\Model;

use App\Lib\Response;

class DeleteModel{
    private $db;
    private $response;
    private $tbUser = "persona";
    private $tbRequest = "solicitudEliminarCuenta";

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function deleteUser(){
        //verfica solicitudes
        $data = $this->db->from($this->tbRequest)
                         ->select(null)
                         ->select('id_persona')
                         ->where("status = 1")
                         ->fetchAll();
        if($data){
            $application_date = date('Y-m-d H:i:s');
            $info = [
                "nombre" => "Usuario eliminado",
                "apellidos" => "Usuario eliminado",
                "correo" => "Usuario eliminado",
                "password" => "Usuario eliminado", 
                "telefono" => "Eliminado",
                "urlImg" => "Usuario eliminado",
                "status" => 2,
                "progreso" => 0
            ];

            $info2 = [
                "status" => 2,
                "fechaEliminacion" => $application_date
            ];

            //elimina usuarios que solicitan eliminacion
            foreach($data as $idP){
                $actualizar = $this->db->update($this->tbUser, $info)
                ->where("id = '$idP->id_persona'")
                ->execute();

                if($actualizar){
                    $actualizar2 = $this->db->update($this->tbRequest, $info2)
                    ->where("id_persona = '$idP->id_persona'")
                    ->execute();
                    if ($actualizar2) {
                        $this->response->result = $actualizar2;
                        $this->response->SetResponse(true, "Exito");
                    } else {
                        $this->response->result = $actualizar2;
                        $this->response->SetResponse(false, "Error al cambiar status de solicitud de eliminación del usuario con id $idP->id_persona");
                    }
                } else {
                    $this->response->result = $actualizar;
                    $this->response->SetResponse(false, "Error al eliminar");
                }
            }
            return $this->response;
        } else {
            $this->response->result = $data;
            return $this->response->SetResponse(false, "No hay solicitudes por el momento");
        }
    }
}
