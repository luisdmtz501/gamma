<?php

namespace App\Model;
use App\Lib\Response;

class CalificacionModel{

    private $tbcalificacion='calilficaciones';
    private $tbEstablishmet='establecimiento';
    private $db;

    public  function __CONSTRUCT($db){
        $this->db = $db;
            $this->response = new Response();
    }

    public function list(){
         $list = $this->db->from($this->tbcalificacion)
                         ->orderBy('idcalilficaciones DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->tbcalificacion)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $list,
            'total' => $total
        ];   
    }

    public function listId($id){
        $obtenerId=$this->db->from($this->tbcalificacion)
                        ->where('idcalilficaciones',$id)
                        ->fetch();
        $this->response->result = $obtenerId;
	    return $this->response->SetResponse(true);   
    }

    public function addCalificacion($data){

        $add=$this->db->insertInto($this->tbcalificacion, $data)
				 ->execute();	
	    if ($add != false) {
            $list = $this->db->from($this->tbcalificacion)
                         ->select(null)
                         ->select('calificacion')
                         ->where('idEstablishment',$data['idEstablishment'])
                         ->groupBy('calificacion')
                         ->orderBy('COUNT(*) DESC')
                         ->fetch();
            $addCal = $this->db->update($this->tbEstablishmet)
                         ->set('calificacion',$list->calificacion)
                         ->where('id',$data['idEstablishment'])
                         ->execute();
            if($addCal =! false){
                $this->response->result = $addCal;
                return $this->response->SetResponse(true,'Agregado con exito');
            }else{
                $this->response->errors='no se ha podido agregar la calificacion';
            return  $this->response->SetResponse(false);
            }
		}else{
            $this->response->errors='Error al agregar la calificacion';
            return  $this->response->SetResponse(false);
        }
    }

    public function updateCalificacion($id){
        $update = $this->db->update($this->tbcalificacion, $data)
                                        ->where('idcalilficaciones',$id)
                                        ->execute();
		
	    if ($update != false) {
            //actualizar la calificacion de establishment
				   $this->response->result = $update;
		    return $this->response->SetResponse(true,'Actualizado con exito');
		}else{
                    $this->response->errors='Error al actualizar los datos';
            return  $this->response->SetResponse(false);
        }
    }

    public function deleteCalificacion($id){
        $eliminar=$this->db->update($this->tbcalificacion)
                          ->set('status', 2)
                          ->where('idcalilficaciones', $id)
                          ->execute();
        if ($update != false) {
            //actualizar la calificacion de establishment
				   $this->response->result = $update;
		    return $this->response->SetResponse(true,'Actualizado con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
       $this->response->result=$eliminar;                          
      return $this->response->SetResponse(true,"datos eliminados");    
    }
}

?>