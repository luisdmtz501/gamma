<?php
namespace App\Model;

use App\Lib\Response;

class VerifyMembership{
    private $db;
    private $response;
    private $tbMem = "membresia";

    public function __CONSTRUCT($db)
    {
        $this->db = $db; 
        $this->response = new Response();
    }

    public function verifyMembership(){

        $data = $this->db->from($this->tbMem)
                     ->select(null)
                     ->select('idMembresia, fechaUltimoPago, fechaExpiracion')
                     ->where("status = 1")
                     ->fetchAll();
        // var_dump($data[0]->idMembresia);
        if($data ){
            foreach($data as $elementos){
                $fechaActual = date('Y-m-d  H:i:s'); // Obtiene la fecha actual en formato "año(Y)-mes(m)-día(d)    hora(H)- minutos(i) o segundos(s)"
                $fechaActual = strtotime($fechaActual);
                if($fechaActual > strtotime($elementos->fechaExpiracion)){
                    $actualizar = $this->db->update("membresia")
                    ->set("status", 2)
                    ->where("idMembresia", $elementos->idMembresia)
                    ->execute();
                    if($actualizar){
                        $this->response->result = $actualizar;
                        $this->response->SetResponse(true, "Exito");
                    }else{
                        $this->response->result = $actualizar;
                        $this->response->SetResponse(false, "Error al expirar membresias.");
                    }
                }else{
                    $this->response->result = [];
                    $this->response->SetResponse(false, "Todas las membresias se encuentran vigentes.");
                }
            }
            return $this->response;
        }else{
            $this->response->result = [];
            return $this->response->SetResponse(false, "No hay ninguna membresia activa en este momento");
        }
    }
}

?>