<?php
namespace App\Model;

use App\Lib\Response;

class PromotionModel{

    private $db;
    private $tbPromocion = 'promocion';
    private $tbPromocionUsada = 'promousada';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }
    
    //Agregar
    //SI FUNCIONA
    public function add($data){
	  $addPromocion = $this->db->insertInto($this->tbPromocion, $data)
					->execute();
		
	    if ($addPromocion != false) {
				   $this->response->result = $addPromocion;
		    return $this->response->SetResponse(true,'Agregado con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }

    // update
    //si funciona
    public function update($data,$id){
        $addPromocion = $this->db->update($this->tbPromocion, $data)
                                        ->where('id',$id)
                                        ->execute();
		
	    if ($addPromocion != false) {
				   $this->response->result = $addPromocion;
		    return $this->response->SetResponse(true,'Actualizado con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }
    //su funciona
    public function list(){
        $listar = $this->db->from($this->tbPromocion)
                         ->select(null)
                         ->select('promocion.id, 
                         IFNULL(promocion.descripcion, "Sin detalle")AS Descripcion,
                         IFNULL(promocion.idEstablecimiento, "Sin detalle") AS idEstablecimiento,
                         IFNULL(promocion.urlImg, "Sin detalle") AS Imagen,
                         IFNULL(promocion.idPeriodo, "Sin detalle") AS Periodo,
                         IFNULL(promocion.titulo, "Sin detalle") AS Titulo,
                         IFNULL(promocion.nota, "Sin detalle") AS Nota')
						 ->where('id')
						 ->orderBy('id   DESC')
						 ->fetchAll();

		if	($listar !=false)	{
				   $this->response->result=['Lista' => $listar];
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe promocion';
			return $this->response->SetResponse(false);
		}
    }
    //SI FUNCIONA
    public function listforEstab($id){
        $lista = $this->db->from($this->tbPromocion)
                         ->select(null)
                         ->select('promocion.id, 
                         IFNULL(promocion.Descripcion, "Sin detalle")AS Descripcion,
                         IFNULL(promocion.idEstablecimiento, "Sin detalle") AS idEstablecimiento,
                         IFNULL(promocion.urlImg, "Sin detalle") AS Imagen,
                         IFNULL(promocion.idPeriodo, "Sin detalle") AS Periodo,
                         IFNULL(promocion.Titulo, "Sin detalle") AS Titulo,
                         IFNULL(promocion.Nota, "Sin detalle") AS Nota,
                         0 as Selecccionada')
						 ->where('idEstablecimiento',$id)
						 ->orderBy('id DESC')
						 ->fetchAll();

		if	($lista !=false){
				   $this->response->result=$lista;
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe promocion';
			return $this->response->SetResponse(false);
		}
    }
    //Si funciona
    public function countPromos($id,$fecha){
        $data = $this->db->from($this->tbPromocionUsada)
                         ->select('idPromocion')
                         ->leftJoin('promocion on promousada.idPromocion = promocion.id')
                         ->where("promocion.idEstablecimiento = $id and promousada.Fecha BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'")
                         ->fetchAll();
        if ($data) {
            $this->response->result=count($data);
            return $this->response->SetResponse(true);
        } else {
            $this->response->result='0';
            return $this->response->SetResponse(true);
        }    
    }

    //SI FUNCIONA   
    public function listPromosEstabFecha($id,$fecha){
        $data = $this->db->from($this->tbPromocionUsada)
                         ->select(null)
                         ->select('Fecha,promocion.Titulo as TituloPromo,promocion.Descripcion as DescripcionPromo,persona.Nombre as Usuario')
                         ->leftJoin('promocion on promocion.id = promousada.idPromocion')
                         ->leftJoin('establecimiento on establecimiento.id = promocion.idEstablecimiento')
                         ->leftJoin('persona on persona.id = promousada.idPersona')
                         ->where("establecimiento.id = $id
                         and (Fecha  between '$fecha 00:00:00' and '$fecha 23:59:59')")
                         ->fetchAll();

               $this->response->result=$data;
        return $this->response->SetResponse(true);
    }

    //Detalle de promociones 
    //si funciona
    public function detail($id){
        $detail = $this->db->from($this->tbPromocion)
                         ->select(null)
                         ->select('promocion.id, 
                         IFNULL(promocion.descripcion, "Sin detalle")AS Descripcion,
                         IFNULL(promocion.idEstablecimiento, "Sin detalle") AS idEstablecimiento,
                         IFNULL(promocion.urlImg, "Sin detalle") AS Imagen,
                         IFNULL(promocion.idPeriodo, "Sin detalle") AS Periodo,
                         IFNULL(promocion.titulo, "Sin detalle") AS Titulo,
                         IFNULL(promocion.nota, "Sin detalle") AS Nota')
                         ->leftJoin('establecimiento on establecimiento.id = promocion.idEstablecimiento')
                         ->where('promocion.id', $id)
                         ->fetch();

        if	($detail != false) {
                   $this->response->result=$detail;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Error, este establecimiento no cuenta con promocion';
            return $this->response->SetResponse(false);
        }
    }
    //Eliminar
    //SI FUNCIONA
    public function delete($idPromocion){
		$eliminar = $this->db->deleteFrom($this->tbPromocion)
				 ->where('id',$idPromocion)
				 ->execute();

		if	($eliminar != false) {
				   $this->response->result=$eliminar;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				   $this->response->errors='Error, al eliminar la promoción, verifique nuevamente!!';
			return $this->response->SetResponse(false);
        }
    }
}