<?php

$app->group('/verify_membership/', function (){
    
    $this->put('verifyMembership', function($req, $res, $args){
        return $res->withHeader('Content.type', 'application/json')
        ->write(
            json_encode($this->model->verify_membership->verifyMembership())
        );
    });
});
?>