<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

    $app->group('/eventos/', function () {

      //obtiene el evento vigente
      $this->get('traerEvento', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->traerEvento())
                   );
      });
      //obtiene el progreso de el usuario del 0 al 7
      $this->get('traerProgreso/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->traerProgreso($args['idPersona']))
                   );
      });
      //registra la poscision del usuario al completar sus actividades
      $this->post('colocarPocision/{idEvento}/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->colocarPocision($args['idEvento'], $args['idPersona']))
                   );
      });
      //trae la posicion en la que quedo el usuario 
      $this->get('obtenerPocision/{idEvento}/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->obtenerPocision($args['idEvento'], $args['idPersona']))
                   );
      });
      //trae los datos del premio
      $this->get('obtenerPremio/{puesto}/{idEvento}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->obtenerPremio($args['puesto'], $args['idEvento']))
                   );
      });
      //marca una actividad como completada
      $this->post('registrarActividadesCompletadas/{idPersona}/{idActividad}/{idEstab}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->registrarActividadesCompletadas($args['idPersona'], $args['idActividad'], $args['idEstab']))
                   );
      });
      //asigna actividades al usuario
      $this->get('obtenerActividades/{idEvento}/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                   ->write(
                      json_encode($this->model->eventos->obtenerActividades($args['idEvento'], $args['idPersona']))
                   );
      });
      //verifica si el usuario ya tiene asignadas actividades
      $this->get('comprobarActividades/{idEvento}/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->comprobarActividades($args['idEvento'], $args['idPersona']))
                );
      });
      //obtiene las actividades una vez ya tiene asignadas
      $this->get('traerListaActividades/{idEvento}/{idPersona}', function($req, $res, $args){
          return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->traerListaActividades($args['idEvento'], $args['idPersona']))
             );
      });
      //Agrega premios.
      $this->post('agregarPremios', function($req, $res, $args){
        return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->generarPremio($req->getParsedBody()))
        );
      });
   
      // reclamar premio.
      $this->put('reclamarPremio/{data}/{id}', function($req, $res, $args){
        return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->reclamarPremio($args['data'], $args['id']))
        );
      });
   
      // //Actualizar status de eveto.
      $this->put('actualizarStatusEvento/{data}/{id}', function($req, $res, $args){
        return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->actualizarStatusEvento($args['data'], $args['id']))
        );
      });
   
      //Actualizar status de eveto.
      $this->put('actualizarStatusActividad/{data}/{id}', function($req, $res, $args){
        return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->actualizarStatusActividad($args['data'], $args['id']))
        );
      });
      //Actualiza el progreso del usuario
      $this->get('actualizarProgreso/{idPersona}/{idEvento}', function($req, $res, $args){
         return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->registroProgreso($args['idPersona'], $args['idEvento']))
                  );
      });
      //Token Push para notificaciones
      $this->post('tokenPush', function($req, $res, $args){
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->tokenPush($req->getParsedBody()))
         );
       });
       //Get Token para notificaciones
       $this->get('getToken/{idPersona}', function($req, $res, $args){
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->getToken($args['idPersona']))
         );
       });
       //Borra token para push notifications
       $this->get('deleteTokenByLogoff/{idPersona}/{token}', function($req, $res, $args){
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->deleteTokenByLogoff($args['idPersona'], $args['token']))
         );
       });
       //Envia notificaciones a uno o todos los usuarios
       $this->post('sendIndiAndGlobalNotifications', function($req, $res, $args){
         $parametros = $req->getParsedBody();
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->sendIndiAndGlobalNotifications($parametros['id'], $parametros['type'], $parametros['title'], $parametros['body'], $parametros['data']))
         );
       });
       //Obtiene los textos para los modal informativos
       $this->post('getInformative', function($req, $res, $args){
         $parametros = $req->getParsedBody();
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->getInformative($parametros))
         );
       });
       //Crea los textos para los modal informativos
       $this->post('createInformative', function($req, $res, $args){
         $parametros = $req->getParsedBody();
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->createInformative($parametros))
         );
       });
       //Actualiza los textos para los modal informativos
       $this->put('updateInformative', function($req, $res, $args){
         $parametros = $req->getParsedBody();
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->updateInformative($parametros))
         );
       });
       //Borra los textos para los modal informativos
       $this->delete('deleteInformative', function($req, $res, $args){
         $parametros = $req->getParsedBody();
         return $res ->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->eventos->deleteInformative($parametros))
         );
       });
    });
?>