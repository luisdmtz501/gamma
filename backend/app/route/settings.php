<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Configuración de mi APP
        'app_token_name'   => 'APP-TOKEN',
        'connectionString' => [
            // 'dns'  => 'mysql:host=127.0.0.1;dbname=gamma;charset=utf8',//localhost
            'dns'  => 'mysql:host=localhost;dbname=u219376423_huauchitourDev;charset=utf8',//dev
            // 'dns'  => 'mysql:host=45.84.204.205;dbname=u219376423_huauchitour;charset=utf8',//prod
            // 'user' => 'gamma',//local
            'user' => 'u219376423_huauchitourDev',//dev
            // 'user' => 'u219376423_huauchitour',//prod
            // 'pass' => 'Stardust88/@Gamma*'//local
            'pass' => 'Stardust88/@Huauchitour*'//prod
        ]
    ],
];
