<?php

$app->group('/delete_user/', function () {
    $this->put('deleteUser', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->delete_user->deleteUser())
                 );
    });
});

?>