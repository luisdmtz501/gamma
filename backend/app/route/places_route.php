<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;
//use App\Model\PlacesModel;

$app->group('/places/', function (){ 

    $this->post('agregarLugar', function ($req, $res, $args) {
    //    $parametros = $req->getParsedBody();
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->places->agregarLugar($req->getParsedBody()))
                  );
    });
    
    
    $this->get('getBy/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->places->getBy($req->getParsedBody(), $args['id']))
    			 );
    });
    
    $this->get('toList', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->places->toList($req->getParsedBody()))
                 );
    });
    
    $this->put('updateLugar/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->places->updateLugar($req->getParsedBody(),$args['id']))
    			 );
    });
    
    $this->delete('delete/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->places->delete($args['id']))
    			 );
    });

})->add(new AuthMiddleware($app));