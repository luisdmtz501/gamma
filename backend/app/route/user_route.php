<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/user/', function () {
    $this->post('registerUser', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->user->registerUser($req->getParsedBody()))
                 );
    });

    $this->get('informationUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->informationUser($args['id']))
    			 );
    });

    $this->put('updateInformationUser/{idPersona}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->user->updateInformationUser($req->getParsedBody(), $args['idPersona']))
    			 );
    });

    $this->put('deleteUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->deleteUser($args['id']))
    			 );
    });

    $this->put('updatePassword/{email}', function($req, $res, $args){
        $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->updatePassword($args['email'], $parametros['password']))
                 );
    });
    
    $this->get('toList/{idTipoPersona}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->toList($args['idTipoPersona']))
                 );
    });

    $this->get('toListMembership/{idPer}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->toListMembership($args['idPer']))
                 );
    });

})->add(new AuthMiddleware($app));