<?php 

use App\Model\App\AuthMiddleware;

$app->group('/calificacion/',function(){
    $this->get('list', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                ->write(
          json_encode($this->model->calificacion->list())
       );
    });

    $this->get('list/{id}', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                ->write(
          json_encode($this->model->calificacion->listId($args['id']))
       );
    });

    $this->post('add', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                ->write(
          json_encode($this->model->calificacion->addCalificacion($req->getParsedBody()))
       );
    });

   $this->put('update/{id}', function ($req, $res, $args){
      return $res->withHeader('Content-type','application/json')
                  ->write(
                     json_encode($this->model->calificacion->actualizarCalificacion($req->getPersedBody()))
                  );
   });

   $this->delete('delete/{id}', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                ->write(
          json_encode($this->model->calificacion->deleteCalificacion($args['id']))
       );
    });
})#->add(new AuthMiddleware($app));
//preguntar si para poder dar una calificacion se necesitara estar logeado

?>