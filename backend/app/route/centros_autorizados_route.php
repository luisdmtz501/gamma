<?php
    $app->group('/centros_autorizados/', function () {
        $this->get('obtenerCentros', function($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                    ->write(
                        json_encode($this->model->centros_autorizados->obtenerCentros())
                    );
        });
    });
?>