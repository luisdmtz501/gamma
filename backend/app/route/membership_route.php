<?php 
use App\Lib\Auth,
	App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/membership/', function(){
	
	$this->post('add', function ($req, $res, $args) {
		$parametros = $req->getParsedBody();
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->membership->add($parametros['correo'], $parametros['plan']))
				   );
	});

	$this->get('list', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->list())
					);
	});
	
	$this->put('update/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->membership->update($req->getParsedBody(), $args['id']))
				   );
	});

    $this->get('detail/{id}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->membership->detail($args['id']))
				   );
	});

    $this->get('detailForId/{id}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->membership->detailForId($args['id']))
				   );
	});
	
	$this->delete('delete/{idMembresia}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					  json_encode($this->model->membership->delete($args['idMembresia']))
					);            
	});

	$this->get('validatePass/{idP}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->validatePass($args['idP']))
					);
	});

	$this->get('buscarQr/{idP}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->buscarQr($args['idP']))
					);
	});

	$this->post('registrarPlan', function ($req, $res, $args) {
		$parametros = $req->getParsedBody();
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->registrarPlan($parametros['nombre'], $parametros['precio'], $parametros['tiempo']))
					);
	});

	$this->get('obtenerPlanes', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->obtenerPlanes())
					);
	});

	$this->get('obtenerPlan/{nombre}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->obtenerPlan($args['nombre']))
					);
	});

	$this->put('actualizarPlan/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->actualizarPlan($req->getParsedBody(), $args['id']))
					);
	});

	$this->delete('eliminarPlan/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->membership->eliminarPlan($args['id']))
					);
	});

})->add(new AuthMiddleware($app));