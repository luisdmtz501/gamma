<?php
// use App\Lib\Auth,
    // App\Lib\Response,
    // App\Validation\userValidation
    // App\Middleware\AuthMiddleware;

$app->group('/auth/', function () {
    
    #login User
    $this->post('user', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->autenticar($parametros['user'],$parametros['password'],1,$token,$plataforma))
                   );
    });
    #login Admin-Estab
    $this->post('adminestab', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->auth->autenticar($parametros['user'],$parametros['password'],2,$token,$plataforma))
                 );
    });
    #login Admin
    $this->post('admin', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->auth->autenticar($parametros['User'],$parametros['Password'],3,$token,$plataforma))
                 );
    });
    #logout
    $this->post('logout', function ($req, $res, $args){
      $parametros = $req->getParsedBody();
      $id = $parametros["id"];
      return $res->withHeader('Content-type','application/json')
              ->write(
                json_encode($this->model->auth->logout($id)
              ));
    });

    #get data token
    $this->get('getData/{token}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->getData($args['token']))
                   );
    });

    #Web - enviar codigo y validar correo
    $this->post('sendCodeEmail', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->sendCodeEmail($parametros['correo'], $parametros['tipoPersona']))
          );
    });

    $this->post('validateCodeEmail/{correo}', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->validateCodeEmail($args['correo'], $parametros['codigo'], $parametros['tipoPersona']))
          );
    });

    $this->post('validateEmail', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->validateEmail($parametros['email'],$parametros['tipoPersona']))
          );
    });
    $this->post('validateCode/{correo}', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->validateCode($args['correo'], $parametros['codigo'],$parametros['tipoPersona']))
          );
    });
    $this->get('checkPassword/{id}/{pass}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->checkPassword($args["id"], $args['pass']))
          );
    });
    $this->post('removalRequest/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->removalRequest($args["id"]))
          );
    });
});
