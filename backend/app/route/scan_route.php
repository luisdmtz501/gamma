<?php 
use App\Lib\Auth,
	App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/scan/', function(){
	

	$this->post('addScan', function ($req, $res, $args) {
		$persona = $req->getParsedBody(); 
		$idPersona = $persona["idPersona"];
		$promocion = $req->getParsedBody(); 
		$idPromocion = $promocion["idPromocion"];
		    return $res->withHeader('Content-type','application/json') 
				       ->write(
				     	  json_encode($this->model->scan->addScan($idPersona,$idPromocion)
				        ));
	});
	// validateMembershi
	$this->get('validateMembership/{id}', function ($req, $res, $args) {
		    return $res->withHeader('Content-type','application/json') 
				       ->write(
				     	  json_encode($this->model->scan->validateMembership($args['id'])
				        ));
	});

})->add(new AuthMiddleware($app));