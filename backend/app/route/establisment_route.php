<?php 
use App\Lib\response,
    App\Middleware\AuthMiddleware;


 $app->group('/establishment/', function () {

 	$this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->establishment->addEstablishment($req->getParsedBody()))
                  );
    });

  	$this->get('obtain/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->obtain($args['id']))
                 );
    });

    $this->get('toListForSubcategories/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->establishment->toListForSubcategories($args['id']))
                  );
    });
    $this->get('toListForCategories/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                   json_encode($this->model->establishment->toListForCategories($args['id']))
                );
    });
    $this->get('searchLike/{like}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                   json_encode($this->model->establishment->searchLike($args['like']))
                );
    });

    $this->put('update/{id}', function($req,$res,$args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                     json_encode($this->model->establishment->update($args['id'],$req->getParsedBody()))
                 );
    });
    $this->put('delete/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->delete($args['id']))
                 );
    });
   //  categorias y subcategorias
    $this->get('listCategories', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->listCategories())
                 );
    });

    $this->get('listCategoriesApp', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->listCategoriesApp())
                 );
    });

    $this->get('listSubcategories/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->listSubcategories($args['id']))
                 );
    });

    $this->post('addCategoria', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->addCategoria($req->getParsedBody()))
                 );
    });

    $this->post('addSubcategoria', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->addSubcategoria($req->getParsedBody()))
                 );
    });

    $this->put('updateCategoria/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->updateCategoria($req->getParsedBody(),$args['id']))
                 );
    });

    $this->put('updateSubcategoria/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->updateSubcategoria($req->getParsedBody(),$args['id']))
                 );
    });

    $this->put('deleteCategoria/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->deleteCategoria($args['id']))
                 );
    });

    $this->put('deleteSubcategoria/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->deleteSubcategoria($args['id']))
                 );
    });
   //  
    $this->get('list', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->list())
                );
   });
   

   $this->get('listAdmin', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->listAdmin())
                );
   });


   $this->get('detailEstablishment/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->detailEstablishment($args['id']))
                );
   });

   $this->get('listapp', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->listApp())
                );
   });
   $this->get('listCentro', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->listCentro())
                );
   });

   $this->get('listexperiencies', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                ->write(
                   json_encode($this->model->establishment->listExperiencies())
                );
   });
   
 });//->add(new AuthMiddleware($app));
