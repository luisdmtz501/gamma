<?php 
use App\Lib\Auth,
	App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/promotion/', function(){
	
	$this->post('add', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->promotion->add($req->getParsedBody()))
				   );
	});

	$this->post('addScan', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->promotion->addScan($req->getParsedBody()))
				   );
	});
	
	$this->put('update/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->promotion->update($req->getParsedBody(), $args['id']))
				   );
	});

    $this->get('detail/{id}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->promotion->detail($args['id']))
				   );
	});
	// countPromos
	$this->get('count/{id}/{fecha}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->promotion->countPromos($args['id'],$args['fecha']))
				   );
	});
	// 
	$this->get('listEstabDate/{id}/{fecha}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->promotion->listPromosEstabFecha($args['id'],$args['fecha']))
				   );
	});
	// 
	$this->delete('delete/{idPromocion}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					  json_encode($this->model->promotion->delete($args['idPromocion']))
					);            
	});
	// 
	$this->get('list', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->promotion->list())
					);
	});

	$this->get('list/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->promotion->listforEstab($args['id']))
					);
	});


})->add(new AuthMiddleware($app));