<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/image/', function () {

  $this->post('add/{tipo}/{id}', function ($req, $res, $args) {
    $file = $_FILES;
    return $res->withHeader('Content-type','application/json')
               ->write(
                 json_encode($this->model->image->add($file, $args['tipo'], $args['id']))
               );
  });

  $this->post('updateUri/{tipo}', function ($req, $res, $args){
    return $res->withHeader('Content-type', 'application/json')
               ->write(
                  json_encode($this->model->image->updateUri($args['tipo']))
               );
  });

});