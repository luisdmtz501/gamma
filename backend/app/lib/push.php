<?php
namespace App\Lib;

class PushExpo{
  public static function EMC($title, $data, $body, $token){
    
    try{
      $payload = array(
        'to' => $token,
        'sound' => 'default',
        'title'=>$title,
        'data'=>$data,
        'body' => $body,
      );
  
    $notificacion= new Expopush();
    $respuesta=$notificacion->sendPushNotificationuser($payload);
    }catch (\Exception $ex){
      $respuesta = $ex;
    }
    return $respuesta;
  }
}
#clase firebase para envio de push
class Expopush {
   /*
   * This function will make the actuall curl request to firebase server
   * and then the message is sent
   */
   public function sendPushNotificationuser($payload) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Accept-Encoding: gzip, deflate",
        "Content-Type: application/json",
        "cache-control: no-cache",
        "host: exp.host"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;
    }
   }
}
?>
