<?php
namespace App\Lib;

class Configuracion{

    // private static $tipoServidor = 'local';
    // private static $tipoServidor = 'dev';
    private static $tipoServidor = 'prod';
    
    private static $url = [
        'local'=>[
            'servidor' => '///Users/humbertomartinez/Sites/gamma/backend/',
            'host'     => 'http://localhost/gamma/backend/'
        ],
        'dev'=> [
            'servidor'  => '/home/u219376423/domains/huauchitour.com/public_html/dev/backend/',
            'host'      => 'https://huauchitour.com/dev/backend/'
        ],
        'prod'=>[
            'servidor'  => '/home/u219376423/domains/huauchitour.com/public_html/backend/',
            'host'      => 'https://huauchitour.com/backend/'
        ]
    ];

    private static $twllio = [
        'local'=>[
            'sid'   => '',
            'token' => '',
            'from'  => ''
        ],
        'dev'=> [
            'sid'   => '',
            'token' => '',
            'from'  => '+'
        ],
        'prod' => [
            'sid'   => '',
            'token' => '',
            'from'  => ''
        ]    
    ];
    
    private static $firebase = [
        'local'=>[
            'key'       =>  '',
            'urlRTDB'   =>  ''
        ],
        'dev'=> [
            'key'       =>  '',
            'urlRTDB'   =>  ''
        ],
        'prod'=>[
            'key'       =>  '',
            'urlRTDB'   =>  ''
        ] 
    ];

    private static $urlRecover = [
        'local'=>[
            'url'       => "http://localhost/gamma/web/recover-passsword/authenticated-code.html"
        ],
        'dev'=> [
            'url'       => "https://lubo.com.mx/go/dev/web/recover-passsword/authenticated-code.html"
        ],
        'prod'=>[
            'url'       => "https://lubo.com.mx/go/web/recover-passsword/authenticated-code.html"
        ] 
    ];

    private static $conekta = [
        'local'=>[
            'key'=>""#dev
        ],
        'dev'=> [
            'key'=>""#dev
        ],
        'prod'=>[
            'key'=>""#prod
        ]
    ];

    private static $facturama = [

    ];
    // funciones get
    public static function getUrl(){
        return self::$url[self::$tipoServidor];
    }
    public static function getTwilio(){
        return self::$twllio[self::$tipoServidor];
    }
    public static function getFirebase(){
        return self::$firebase[self::$tipoServidor];
    }
    public static function getUrlRecover(){
        return self::$urlRecover[self::$tipoServidor];
    }
}