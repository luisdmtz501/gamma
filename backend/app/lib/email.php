<?php
namespace App\Lib;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email
{
	static function Send($email,$cadena)
	{
		#enviar el correo
		$titulo = 'Tu codigo de verificacion de correo para HuauchiTour';
		$mensaje = "<!DOCTYPE html>
		<head>
		</head>
		<body>
		<div style='margin:0;padding:0'>
		<table width='100%' cellpadding='0' cellspacing='0' style='padding:0;margin:0'>
		  <tbody><tr>
			<td style='font-size:0'><span></span></td>
			<td valign='top' align='left' style='width:640px;max-width:640px'>
				<table width='100%' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0' style='padding:0;margin:0;border:0'>
					<tbody><tr>
					  <td align='left' style='padding:32px 20px 0 20px' id='m_5592586648109469298main-pad'>
		
						  <center><a target='_blank'></a> <img alt='' src='http://huauchitour.com/dev/backend/img/logos/logo-PNG.png' width='140' data-image-whitelisted='' class='CToWUd'></a>
						  </center>
						  
						  <h1 style='font-family:Helvetica,Arial,sans-serif;font-size:24px;line-height:31px;color:#777777;padding:0;margin:28px 0 32px 0;font-weight:400;text-align:center;text-decoration:none'><span style='display:block'> ¡BIENVENIDO AL TOUR! </span></a></h1>
		
						  
						  
						  <p style='font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:20px;color:#333333;margin:0;padding:0;margin:0 0 20px 0;text-align:justify;'>
							Te damos la bienvenida a HuauchiTour®. <br>Estamos muy contentos porque ahora formarás parte de nuestra comunidad.</strong>
						  </p>
						  
		
						  <p align='center' style='color: #e6078e; font-size: 30px;'>Tu codigo de verificación de correo es:</p>	
						  <p align='center' style='color: #e6078e; font-size: 30px; font-weight: bold;'>$cadena</p>				
		
						  <p style='font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:20px;columns: #333333;padding:0;margin:33px 0 20px 0;text-align:justify;'> Este código tiene una vigencia de 24 horas. si no realizaste esta acción porfavor haz caso omiso a este correo 
						  </p>
		
						  <p style='font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:20px;columns: #333333;margin:0;padding:0;margin:0 0 20px 0;text-align:justify;'>
							 Si tienes alguna pregunta o inquietud, contacta con nuestro equipo de soporte <a href='' style='font-size:16px;line-height:20px;color:#007bff;text-decoration:none' target='_blank'></a>
						  </p>
						  
		
						  
						  <p style='font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:20px;columns: #333333;padding:0;margin:35px 0 0 0;text-align:left'>
							Saludos cordiales,<br>El Equipo de HuauchiTour®
						  </p>
						  
		
					  </td>
					</tr>
				</tbody></table>
			</td>
			<td style='font-size:0'><span></span></td>
		  </tr>

		  <tr>
			<td style='font-size:0'><span></span></td>
			<td valign='middle' align='center' style='width:640px;max-width:640px;padding:25px 0 28px 0' id='m_5592586648109469298copyrights-block'>
				<center><img src='http://huauchitour.com/dev/backend/img/logos/logo-PNG.png' alt='Logo Huauchitour' width='70px' height='70px'></center>
				<p style='font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;color:#999999;padding:0;margin:4px 0 22px 0'>HuauchiTour® 2023 <br>
					Startdust inc S.A de C.V</p>
				<table cellpadding='0' cellspacing='0' style='padding:0;margin:0;border:0'>
					<tbody><tr>
						
					</tr>
				</tbody></table>
			</td>
			<td style='font-size:0'><span></span></td>
		  </tr>
		</tbody></table><div class='yj6qo'></div>
		
		</body>	";

		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Cabeceras adicionales
		$cabeceras .= 'From: HuauchiTour <contacto@huauchitour.com>' . "\r\n";
		$cabeceras .= "Cc: $email" . "\r\n";

		$mail = new PHPMailer;
   		$mail->isSMTP();
   		$mail->SMTPDebug = 2;
   		$mail->Host = 'smtp.hostinger.com';
   		$mail->Port = 587;
   		$mail->SMTPAuth = true;
		$mail->CharSet = 'UTF-8'; 
   		$mail->Username = 'contacto@huauchitour.com';
   		$mail->Password = 'Huauchitour1.';
   		$mail->setFrom('contacto@huauchitour.com', 'Contacto Huauchitour');
   		$mail->addReplyTo('contacto@huauchitour.com', 'Contacto Huauchitour');
   		$mail->addAddress("$email", " ");
		$mail->isHTML(true); 
   		$mail->Subject = "$titulo";
   		$mail->Body = $mensaje;
   		//$mail->addAttachment('attachment.txt');
   		
		if ($mail->send()) {
		// if (mail($email, $titulo, $mensaje, $cabeceras)){
			return '1';
		}else{
			return '0';
		}
	}
}
?>