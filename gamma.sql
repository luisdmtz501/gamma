-- MySQL dump 10.13  Distrib 5.7.38, for Linux (x86_64)
--
-- Host: 45.84.204.205    Database: u219376423_huauchitourDev
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.12-MariaDB-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calilficaciones`
--

DROP TABLE IF EXISTS `calilficaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calilficaciones` (
  `idcalilficaciones` int(11) NOT NULL AUTO_INCREMENT,
  `idEstablishment` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `calificacion` double NOT NULL,
  `comentario` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `status` enum('activo','inactivo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activo',
  PRIMARY KEY (`idcalilficaciones`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calilficaciones`
--

LOCK TABLES `calilficaciones` WRITE;
/*!40000 ALTER TABLE `calilficaciones` DISABLE KEYS */;
INSERT INTO `calilficaciones` VALUES (1,2,1,4,'','2022-06-22 15:43:32','activo'),(2,4,1,5,NULL,'2022-06-22 15:43:32','activo'),(3,5,1,3,'','2022-06-22 15:43:32','activo'),(4,2,28,4,'','2022-06-22 15:43:32','activo'),(5,2,1,4,'','2022-06-22 15:43:32','activo'),(6,2,1,4,'','2022-06-22 15:43:32','activo'),(7,2,1,4,NULL,'2022-06-22 15:43:32','activo'),(8,2,28,4,'','2022-06-22 15:43:32','activo'),(9,3,28,4,'','2022-06-22 15:43:32','activo'),(10,3,28,4,'','2022-06-22 15:43:32','activo'),(11,3,28,4,'','2022-06-22 15:43:32','activo'),(12,3,28,4,'','2022-06-22 15:43:32','activo'),(13,3,28,4,'','2022-06-22 15:43:32','activo'),(14,3,28,4,'','2022-06-22 15:43:32','activo'),(15,3,28,4,'','2022-06-22 15:43:32','activo'),(16,3,28,4,'','2022-06-22 15:43:32','activo'),(17,2,28,4,'','2022-06-22 15:43:32','activo'),(18,2,28,4,'','2022-06-22 15:43:32','activo'),(19,2,28,4,'','2022-06-22 15:43:32','activo'),(20,7,1,5,'','2022-06-22 15:43:32','activo'),(21,2,1,1,'','2022-06-22 15:43:32','activo'),(22,2,1,4,'','2022-06-22 15:43:32','activo'),(23,2,1,4,'','2022-06-22 15:43:32','activo'),(24,2,1,4,'','2022-06-22 15:43:32','activo'),(25,2,1,4,'','2022-06-22 15:43:32','activo'),(26,2,1,3,'','2022-06-22 15:43:32','activo'),(27,2,1,3,'','2022-06-22 15:43:32','activo');
/*!40000 ALTER TABLE `calilficaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `urlImg` varchar(450) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (7,'Comida',NULL,'activo'),(8,'Vinos y Licores',NULL,'inactivo'),(9,'update del servicio 2','','inactivo');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_has_subcategorias`
--

DROP TABLE IF EXISTS `categoria_has_subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_has_subcategorias` (
  `idEstablecimiento` int(11) NOT NULL,
  `idSubCategorias` int(11) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`idEstablecimiento`,`idSubCategorias`),
  KEY `fk_Establecimiento_has_SubCategorias_SubCategorias1_idx` (`idSubCategorias`),
  KEY `fk_Establecimiento_has_SubCategorias_Establecimiento1_idx` (`idEstablecimiento`),
  CONSTRAINT `fk_Establecimiento_has_SubCategorias_Establecimiento1` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`id`),
  CONSTRAINT `fk_Establecimiento_has_SubCategorias_SubCategorias1` FOREIGN KEY (`idSubCategorias`) REFERENCES `subcategorias` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_has_subcategorias`
--

LOCK TABLES `categoria_has_subcategorias` WRITE;
/*!40000 ALTER TABLE `categoria_has_subcategorias` DISABLE KEYS */;
INSERT INTO `categoria_has_subcategorias` VALUES (1,1,'activo'),(2,1,'activo'),(3,2,'activo'),(4,2,'activo'),(5,2,'activo'),(6,2,'activo'),(7,2,'activo'),(8,2,'activo'),(9,1,'activo'),(10,1,'activo');
/*!40000 ALTER TABLE `categoria_has_subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigoQrFisico`
--

DROP TABLE IF EXISTS `codigoQrFisico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigoQrFisico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `fk_codigoQrFisico_1_idx` (`idPersona`),
  CONSTRAINT `fk_codigoQrFisico_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigoQrFisico`
--

LOCK TABLES `codigoQrFisico` WRITE;
/*!40000 ALTER TABLE `codigoQrFisico` DISABLE KEYS */;
/*!40000 ALTER TABLE `codigoQrFisico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigoQrVirtual`
--

DROP TABLE IF EXISTS `codigoQrVirtual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigoQrVirtual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `fk_CodigoQr_1_idx` (`idPersona`),
  CONSTRAINT `fk_CodigoQr_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigoQrVirtual`
--

LOCK TABLES `codigoQrVirtual` WRITE;
/*!40000 ALTER TABLE `codigoQrVirtual` DISABLE KEYS */;
INSERT INTO `codigoQrVirtual` VALUES (26,27,'activo'),(27,1,'activo'),(28,2,'activo'),(29,3,'activo'),(30,28,'activo'),(59,63,'activo'),(61,66,'activo'),(63,70,'activo'),(64,71,'activo'),(65,67,'activo'),(66,72,'activo'),(67,73,'activo'),(68,65,'activo'),(69,78,'activo'),(70,77,'activo');
/*!40000 ALTER TABLE `codigoQrVirtual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establecimiento`
--

DROP TABLE IF EXISTS `establecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `establecimiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `info` varchar(450) DEFAULT NULL,
  `calificacion` double DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `urlImg` varchar(450) DEFAULT NULL,
  `urlImgPerfil` varchar(450) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `longitud` varchar(100) DEFAULT NULL,
  `latitud` varchar(100) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  `telefono` varchar(10) DEFAULT NULL,
  `centroAutorizado` enum('y','n') DEFAULT NULL,
  `tipo` enum('establecimiento','experiencia') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_establecimiento_persona` (`idPersona`),
  CONSTRAINT `fk_establecimiento_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establecimiento`
--

LOCK TABLES `establecimiento` WRITE;
/*!40000 ALTER TABLE `establecimiento` DISABLE KEYS */;
INSERT INTO `establecimiento` VALUES (1,'prueba con update','centro','',0,'Juárez 12, Centro',NULL,'http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'787','7788.225','activo','7767627045',NULL,'experiencia'),(2,'pizza_napoli','las mejores pizzas','Ven a disfrutar una de la gran variedad de pizzas con las que contamos ',4,'Juárez 12, Centro','http://huauchitour.com/dev/backend/img/logos/fXW4uBWDbk_2.jpg','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'-98.05003','20.17443','activo','7767628115',NULL,'experiencia'),(3,'Tienda tekate','tienda de cerverzas','',0,'Matamoros 63','http://huauchitour.com/dev/backend/img/logos/fEtSWX0gTI_3.png','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,NULL,NULL,'activo','7764663375',NULL,'experiencia'),(4,'Modelorama','variedad de bebidas','',0,'Huauchinango, Centro','http://huauchitour.com/dev/backend/img/logos/8AdTK3lEOW_4.png','',2,NULL,NULL,'activo','8004663301',NULL,'experiencia'),(5,'Tako keto','corregidora','Ven a deleitarte con nuestros cafés, acompañados de unas deliciosas donas de Krispy Cream®, o de algunos de nuestros pays, tenemos variedad para todo tipo de antojo.',0,'Cuauhtémoc 22, Centro','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg','',2,'-98.051316','20.173426','activo','2216413774',NULL,'establecimiento'),(6,'Modelorama','centro','Ven a deleitarte con nuestros cafés, acompañados de unas deliciosas donas de Krispy Cream®, o de algunos de nuestros pays, tenemos variedad para todo tipo de antojo.',0,'Av. de los Técnicos, Rancho Viejo','http://huauchitour.com/dev/backend/img/logos/uaenCijTDH_6.png','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'-98.069846','20.183573','activo','5522660002',NULL,'establecimiento'),(7,'Stardust_stars','centro','',0,'Huauchinango, Centro','http://huauchitour.com/dev/backend/img/logos/wdICOzYrom_7.jpeg','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'','','activo','7767628831',NULL,'establecimiento'),(8,'Stardust_star1','centro','',0,'Corregidora 45, Centro','http://huauchitour.com/dev/backend/img/logos/8WtB5VnMj7_8.jpeg','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'-98.051666','20.173353','activo','7767629403',NULL,'establecimiento'),(9,'Stardust_star','centro','',0,'Corregidora 45, Centro','http://huauchitour.com/dev/backend/img/logos/xdqbeAzTXS_9.jpeg','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'-98.051666','20.173353','activo','7761052289',NULL,'experiencia'),(10,'prueba 10','centro','',0,'','http://huauchitour.com/dev/backend/img/logos/AifT4BmFW5_10.png','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'787','7788.225','activo','',NULL,'experiencia'),(11,'prueba 11','centro','esta es una prueba sin img',2,'','','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'20.17883','-98.05220','activo','','','establecimiento'),(12,'prueba','centro','',4,'Colonia Centro','http://huauchitour.com/dev/backend/img/logos/AifT4BmFW5_10.png','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'','','activo','7641234568','','experiencia'),(13,'prueba 12','centro ','prueba sin img',0,'Colonia Centro','http://huauchitour.com/dev/backend/img/logos/AifT4BmFW5_10.png','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'20.17499','-98.05232','activo','7761234560','','establecimiento'),(14,'prueba2','centro','',5,'Juárez 12, Centro','','http://huauchitour.com/dev/backend/img/logos/LYk7X4NIf5_5.jpeg',2,'20.17441','-98.05254','activo','7641234561','','establecimiento');
/*!40000 ALTER TABLE `establecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membresia`
--

DROP TABLE IF EXISTS `membresia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membresia` (
  `idMembresia` int(11) NOT NULL AUTO_INCREMENT,
  `fechaUltimoPago` datetime DEFAULT current_timestamp(),
  `fechaExpiracion` datetime DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`idMembresia`),
  KEY `fk_membresia_persona_idx` (`idPersona`),
  CONSTRAINT `fk_membresia_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membresia`
--

LOCK TABLES `membresia` WRITE;
/*!40000 ALTER TABLE `membresia` DISABLE KEYS */;
INSERT INTO `membresia` VALUES (1,'2021-07-04 00:00:00','2021-07-05 00:00:00',1,'activo'),(2,'2021-07-05 00:00:00','2021-07-07 00:00:00',2,'activo'),(4,'2022-07-04 00:00:00','0202-08-02 00:00:00',3,'activo'),(5,'2022-09-04 21:30:16','2022-10-02 21:30:16',70,'inactivo'),(6,'2022-09-05 11:22:29','2022-10-03 11:22:29',3,'activo'),(7,'2022-09-05 11:23:19','2022-10-03 11:23:19',70,'activo');
/*!40000 ALTER TABLE `membresia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensajes`
--

DROP TABLE IF EXISTS `mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensajes` (
  `idmensajes` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL,
  `fechaAlta` datetime NOT NULL,
  `fechaExpiracion` datetime NOT NULL,
  `temcorreo` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  `tipoPersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmensajes`),
  KEY `fk_mensajes_1_idx` (`tipoPersona`),
  CONSTRAINT `fk_mensajes_1` FOREIGN KEY (`tipoPersona`) REFERENCES `tipopersona` (`idTipoPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensajes`
--

LOCK TABLES `mensajes` WRITE;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
INSERT INTO `mensajes` VALUES (197,'b2oPta','2022-09-02 15:43:53','2022-09-03 15:43:53','berthahdz.aranda21@gmail.com','inactivo',1),(198,'xRHYi9','2022-09-02 16:48:36','2022-09-03 16:48:36','berthahdz.aranda21@gmail.com','activo',1),(199,'1sFTpq','2022-09-02 16:53:08','2022-09-03 16:53:08','berthahdz.aranda21@gmail.com','inactivo',1),(200,'4CjTDc','2022-09-02 17:27:31','2022-09-03 17:27:31','jozejimenezdiego@gmail.com','activo',2),(201,'wtMcbq','2022-09-02 17:27:40','2022-09-03 17:27:40','jozejimenezdiego@gmail.com','activo',2),(202,'438611','2022-09-02 17:29:30','2022-09-03 17:29:30','jozejimenezdiego@gmail.com','activo',2),(203,'667094','2022-09-02 17:29:57','2022-09-03 17:29:57','berthahdz.aranda21@gmail.com','activo',2),(204,'606621','2022-09-02 17:31:30','2022-09-03 17:31:30','hernandezaranda.bertha0@gmail.com','activo',2),(205,'LhFulH','2022-09-02 17:35:10','2022-09-03 17:35:10','hernandezaranda.bertha0@gmail.com','activo',2),(206,'kiZCVP','2022-09-02 17:40:21','2022-09-03 17:40:21','berthahdz.aranda21@gmail.com','activo',2),(207,'tJwQod','2022-09-02 18:07:44','2022-09-03 18:07:44','berthahdz.aranda21@gmail.com','activo',1),(208,'OaU5qR','2022-09-02 18:08:32','2022-09-03 18:08:32','berthahdz.aranda21@gmail.com','activo',1),(209,'riVfZ2','2022-09-02 18:10:56','2022-09-03 18:10:56','berthahdz.aranda21@gmail.com','activo',1),(210,'KATfjo','2022-09-03 12:19:34','2022-09-04 12:19:34','berthahdz.aranda21@gmail.com','inactivo',1),(211,'AZkL75','2022-09-05 21:31:27','2022-09-06 21:31:27','berthahdz.aranda21@gmail.com','activo',1),(212,'UnKjZS','2022-09-05 21:32:31','2022-09-06 21:32:31','berthahdz.aranda21@gmail.com','activo',1),(213,'9bz5bL','2022-09-05 21:34:21','2022-09-06 21:34:21','berthahdz.aranda21@gmail.com','activo',1),(214,'CzWqdZ','2022-09-05 21:36:20','2022-09-06 21:36:20','berthahdz.aranda21@gmail.com','activo',1),(215,'LCsZHb','2022-09-05 21:43:55','2022-09-06 21:43:55','berthahdz.aranda21@gmail.com','activo',1),(216,'552045','2022-09-05 21:53:16','2022-09-06 21:53:16','berthahdz.aranda21@gmail.com','activo',1),(217,'494025','2022-09-05 21:56:44','2022-09-06 21:56:44','berthahdz.aranda21@gmail.com','activo',1),(218,'205150','2022-09-05 22:12:46','2022-09-06 22:12:46','berthahdz.aranda21@gmail.com','activo',1),(219,'603875','2022-09-05 22:13:25','2022-09-06 22:13:25','berthahdz.aranda21@gmail.com','activo',1),(220,'803437','2022-09-05 22:22:33','2022-09-06 22:22:33','berthahdz.aranda21@gmail.com','activo',1),(221,'242818','2022-09-07 01:07:16','2022-09-08 01:07:16','iriislora13@gmail.com','activo',1);
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `idPeriodo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `Status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`idPeriodo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES (1,'Libre','activo'),(2,'Dia','activo'),(3,'Semana','activo'),(4,'Mes','activo');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(150) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `urlImg` varchar(450) DEFAULT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `tipoPersona` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_persona_1_idx` (`tipoPersona`),
  CONSTRAINT `fk_persona_1` FOREIGN KEY (`tipoPersona`) REFERENCES `tipopersona` (`idTipoPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Luis','Dionisio Martinez','luisdionisiomtz501@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','4443382696','','',1),(2,'Luis','Martinez','luis@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','4443382696',NULL,'',3),(3,'Mario','Dionisio Martinez','mario@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','4443382696',NULL,'activo',2),(27,'Bertha','Hdez. Aranda','berthaaranda@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7841231770',NULL,'activo',2),(28,'Bertha','Hdz Añ','bertrtrt5@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7841231770','http://huauchitour.com/dev/backend/img/perfil/t3lnBRfAKt_28.jpg','activo',1),(63,'Eli','Martínez Cayetano ','eliamtzc9908@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',NULL,'http://huauchitour.com/dev/backend/img/perfil/1831982188_63.jpg','activo',1),(64,'Alan','Perez Hernandez','alan.kishin92@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',NULL,NULL,'activo',1),(65,'UwU','n.n','fejedoo@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',NULL,NULL,'activo',1),(66,'Iris ','Lora ','iriislora12@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',NULL,NULL,'activo',1),(67,'Carina','Rivera ','irislora17@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761340136','http://huauchitour.com/dev/backend/img/perfil/c4UbtZm2qy_67.jpeg','activo',1),(69,'Iris','Lora','iriscarinalora@gmail.com','$6$rounds=5000$@startDustH00l10$SaYwpJMPnexK0QNhcqMqQFdEfK.rhWvqxVlGcv4tIvETfl1WZhTOJNUCqqpPeRRG8i.CAh5CSCAd.yOJ45Q6K1','7654321234',NULL,'activo',1),(70,'Naye','Islas','naye.28.is.rod@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761275003',NULL,'activo',1),(71,'Carls','Islas','tedcais@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761275003',NULL,'activo',1),(72,'Elia ','Martínez Cayetano ','elia_9185@hotmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','4447065471','http://huauchitour.com/dev/backend/img/perfil/9m7K5jy4nK_72.jpg','activo',1),(73,'Dulce Karen','Barrera otero','dulcecaren18@hotmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761057749',NULL,'activo',1),(77,'Ana','Islas','naye.28.jul.is@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761275003',NULL,'activo',1),(78,'Martin ','González ','martingb53@hotmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','7761231329','','activo',1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promocion`
--

DROP TABLE IF EXISTS `promocion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEstablecimiento` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `nota` varchar(250) DEFAULT NULL,
  `urlImg` varchar(450) DEFAULT NULL,
  `idPeriodo` int(11) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `fk_promocion_establecimiento_idx` (`idEstablecimiento`),
  KEY `fk_promocion_periodo` (`idPeriodo`),
  CONSTRAINT `fk_promocion_establecimiento` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`id`),
  CONSTRAINT `fk_promocion_periodo` FOREIGN KEY (`idPeriodo`) REFERENCES `periodo` (`idPeriodo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promocion`
--

LOCK TABLES `promocion` WRITE;
/*!40000 ALTER TABLE `promocion` DISABLE KEYS */;
INSERT INTO `promocion` VALUES (1,1,'Sansarita','1 frasco de morita','hola soy prueba 1',NULL,1,'activo'),(2,2,'Tacos','2 tacos por 1','hola soy prueba 2',NULL,2,'activo'),(3,1,'Tortas','1 refresco 1 L. por 3 tortas','hola soy prueba 3',NULL,3,'activo'),(4,1,'Hola','Prueba','hola soy prueba 1',NULL,1,'activo'),(5,1,'nuevo','Prueba','hola soy prueba 9','',1,'activo'),(6,1,'Hola','Prueba','hola soy prueba 8','',1,'activo');
/*!40000 ALTER TABLE `promocion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promousada`
--

DROP TABLE IF EXISTS `promousada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promousada` (
  `idPromocion` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT current_timestamp(),
  KEY `fk_promousada_promocion_idx` (`idPromocion`),
  KEY `fk_promousada_persona_idx` (`idPersona`),
  CONSTRAINT `fk_promousada_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`),
  CONSTRAINT `fk_promousada_promocion1` FOREIGN KEY (`idPromocion`) REFERENCES `promocion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promousada`
--

LOCK TABLES `promousada` WRITE;
/*!40000 ALTER TABLE `promousada` DISABLE KEYS */;
INSERT INTO `promousada` VALUES (2,1,'2021-07-10 02:13:06'),(1,2,'2021-07-10 02:13:18'),(3,2,'2021-07-10 02:13:27');
/*!40000 ALTER TABLE `promousada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperarpassword`
--

DROP TABLE IF EXISTS `recuperarpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperarpassword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL,
  `fechaAlta` datetime DEFAULT NULL,
  `fechaExpiracion` datetime DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  `idPersona` int(11) DEFAULT NULL,
  `tipoPersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_recuperarcontraseña_persona` (`idPersona`),
  CONSTRAINT `fk_recuperarcontraseña_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperarpassword`
--

LOCK TABLES `recuperarpassword` WRITE;
/*!40000 ALTER TABLE `recuperarpassword` DISABLE KEYS */;
INSERT INTO `recuperarpassword` VALUES (1,'xNeY5V','2021-06-30 13:19:00','2021-07-01 13:19:00','luisdionisiomtz501@gmail.com','',1,NULL),(2,'kkayON','2021-07-06 16:08:35','2021-07-06 16:09:35','luisdionisiomtz501@gmail.com','',1,NULL),(3,'n1GMdb','2021-07-06 16:13:46','2021-07-07 16:13:46','luisdionisiomtz501@gmail.com','activo',1,NULL),(4,'UJKPk2','2021-07-06 16:15:42','2021-07-06 16:16:06','luisdionisiomtz501@gmail.com','',1,NULL),(5,'nVsCMJ','2022-03-16 14:45:52','2022-03-17 14:45:52','luisdionisiomtz501@gmail.com','activo',1,NULL),(6,'gDDXkt','2022-03-16 14:45:59','2022-03-17 14:45:59','luisdionisiomtz501@gmail.com','inactivo',1,2),(7,'110821','2022-09-05 20:54:04','2022-09-06 20:54:04','bertrtrt5@gmail.com','activo',28,1),(8,'161515','2022-09-05 21:06:35','2022-09-06 21:06:35','bertrtrt5@gmail.com','activo',28,1);
/*!40000 ALTER TABLE `recuperarpassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategorias`
--

DROP TABLE IF EXISTS `subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `urlImg` varchar(450) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `fk_subcategorias_tipoestablecimiento1_idx` (`idCategoria`),
  CONSTRAINT `fk_subcategorias_tipoestablecimiento1` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategorias`
--

LOCK TABLES `subcategorias` WRITE;
/*!40000 ALTER TABLE `subcategorias` DISABLE KEYS */;
INSERT INTO `subcategorias` VALUES (1,'add prueba 1',9,'','activo'),(2,'Tacos',7,NULL,'activo'),(7,'Cerveza',8,NULL,'inactivo'),(9,'add prueba 1',9,'','inactivo');
/*!40000 ALTER TABLE `subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopersona`
--

DROP TABLE IF EXISTS `tipopersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopersona` (
  `idTipoPersona` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) NOT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`idTipoPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopersona`
--

LOCK TABLES `tipopersona` WRITE;
/*!40000 ALTER TABLE `tipopersona` DISABLE KEYS */;
INSERT INTO `tipopersona` VALUES (1,'Usuario','activo'),(2,'Administrador_Establecimiento','activo'),(3,'Administrador_General','activo');
/*!40000 ALTER TABLE `tipopersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokenpush`
--

DROP TABLE IF EXISTS `tokenpush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokenpush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `plataforma` varchar(100) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `status` enum('activo','inactivo') DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `fk_tokenPush_persona_idx` (`idPersona`),
  CONSTRAINT `fk_tokenPush_persona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokenpush`
--

LOCK TABLES `tokenpush` WRITE;
/*!40000 ALTER TABLE `tokenpush` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokenpush` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'u219376423_huauchitourDev'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-20  9:31:50
